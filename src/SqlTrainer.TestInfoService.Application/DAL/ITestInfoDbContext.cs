﻿using Application;
using Microsoft.EntityFrameworkCore;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.DAL;

public interface ITestInfoDbContext : IApplicationDbContext
{
    DbSet<Topic> Topics { get; }
    DbSet<Test> Tests { get; }
    DbSet<Question> Questions { get; }
    DbSet<TestQuestion> TestQuestions { get; }
    DbSet<CorrectAnswer> CorrectAnswers { get; }
    DbSet<ScheduleTest> ScheduleTests { get; }
    DbSet<UserScheduleTest> UserScheduleTests { get; }
    DbSet<UserAnswer> UserAnswers { get; }
}