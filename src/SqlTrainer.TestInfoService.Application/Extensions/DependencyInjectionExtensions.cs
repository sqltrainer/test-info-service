﻿using System.Reflection;
using Application.Mediator.Extensions;
using Microsoft.Extensions.DependencyInjection;

namespace SqlTrainer.TestInfoService.Application.Extensions;

public static class DependencyInjectionExtensions
{
    public static void AddApplication(this IServiceCollection services)
    {
        services.InjectRequestStuff(Assembly.GetExecutingAssembly());
    }
}