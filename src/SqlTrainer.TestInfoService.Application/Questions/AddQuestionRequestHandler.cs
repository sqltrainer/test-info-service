﻿using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Questions;

public sealed record AddQuestionRequest(
    string Body,
    int Complexity,
    Guid TopicId,
    Guid DatabaseId,
    string CorrectAnswerBody) : IRequest;

public interface IAddQuestionRequestHandler : IRequestHandler<AddQuestionRequest, Guid>;

public sealed class AddQuestionRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<AddQuestionRequestHandler> logger)
    : IAddQuestionRequestHandler
{
    public async Task<IFinalResult<Guid>> HandleAsync(AddQuestionRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await ValidateAsync(request, cancellationToken);
            if (validationResult.IsFailed)
                return validationResult;
            
            var question = new Question
            {
                Body = request.Body,
                Complexity = request.Complexity,
                TopicId = request.TopicId,
                DatabaseId = request.DatabaseId
            };
            await dbContext.Questions.AddAsync(question, cancellationToken);

            var correctAnswer = new CorrectAnswer
            {
                Body = request.CorrectAnswerBody,
                QuestionId = question.Id
            };
            await dbContext.CorrectAnswers.AddAsync(correctAnswer, cancellationToken);
            
            await dbContext.SaveChangesAsync(cancellationToken);

            validationResult.Complete(question.Id);
            
            return validationResult;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Question has not been added. Request: {Request}", request);
            return UnhandledFailedResult.Create<Guid>(ex, "An error occurred while adding the question");
        }
    }

    private async Task<ValidationResult<Guid>> ValidateAsync(AddQuestionRequest request, CancellationToken cancellationToken = default)
    {
        var validationResult = ValidationResult.Create<Guid>();
        
        if (string.IsNullOrWhiteSpace(request.Body))
            validationResult.Fail(nameof(request.Body), "Body is required");
        
        if (string.IsNullOrWhiteSpace(request.CorrectAnswerBody))
            validationResult.Fail(nameof(request.CorrectAnswerBody), "Correct answer body is required");
        
        if (request.Complexity < 1)
            validationResult.Fail(nameof(request.Complexity), "Complexity must be greater than 0");
        
        if (request.TopicId == default)
            validationResult.Fail(nameof(request.TopicId), "Topic id is required");
        
        if (request.DatabaseId == default)
            validationResult.Fail(nameof(request.DatabaseId), "Database id is required");
        
        if (!await dbContext.Topics.AnyAsync(t => t.Id == request.TopicId, cancellationToken))
            validationResult.Fail(nameof(request.TopicId), "Topic does not exist");

        return validationResult;
    }
}