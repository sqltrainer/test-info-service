﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;

namespace SqlTrainer.TestInfoService.Application.Questions;

public sealed record DeleteQuestionRequest(Guid Id) : IRequest;

public interface IDeleteQuestionRequestHandler : IRequestHandler<DeleteQuestionRequest>;

public sealed class DeleteQuestionRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<DeleteQuestionRequestHandler> logger)
    : IDeleteQuestionRequestHandler
{
    public async Task<IFinalResult> HandleAsync(DeleteQuestionRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var question = await dbContext.Questions.FirstOrDefaultAsync(q => q.Id == request.Id, cancellationToken: cancellationToken);
            if (question is null)
                return Result.Failed("Question does not exist");

            dbContext.Questions.Remove(question);
            await dbContext.SaveChangesAsync(cancellationToken);

            return Result.Success();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Question has not been deleted. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "An error occurred while deleting the question");
        }
    }
}