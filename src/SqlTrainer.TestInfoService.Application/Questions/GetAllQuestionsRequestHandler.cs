﻿using Application;
using Application.Mediator.Extensions;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Questions;

public sealed record GetAllQuestionsRequest(
    string SearchTerm, 
    string OrderBy, 
    SortDirection OrderByDirection, 
    int Page, 
    int PageSize) 
    : GetManyRequest(SearchTerm, OrderBy, OrderByDirection, Page, PageSize);

public interface IGetAllQuestionsRequestHandler : IRequestHandler<GetAllQuestionsRequest, IReadOnlyCollection<Question>>;

public sealed class GetAllQuestionsRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<GetAllQuestionsRequestHandler> logger)
    : IGetAllQuestionsRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<Question>>> HandleAsync(GetAllQuestionsRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var query = dbContext.Questions.AsNoTracking();
            
            if (!string.IsNullOrWhiteSpace(request.SearchTerm))
                query = query.Where(q => q.Body.ToLower().Contains(request.SearchTerm.ToLower()));

            var questions = await query
                .SortAndPaginate(request)
                .Include(q => q.CorrectAnswer)
                .ToListAsync(cancellationToken);
            
            return Result.Success(questions);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Questions has not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<IReadOnlyCollection<Question>>(ex, "An error occurred while retrieving the questions");
        }
    }
}