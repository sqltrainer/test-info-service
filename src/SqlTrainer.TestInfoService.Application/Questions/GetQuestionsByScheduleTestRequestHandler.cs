﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Questions;

public sealed record GetQuestionsByScheduleTestRequest(Guid ScheduleTestId) : IRequest;

public interface IGetQuestionsByScheduleTestRequestHandler : IRequestHandler<GetQuestionsByScheduleTestRequest, IReadOnlyCollection<Question>>;

public sealed class GetQuestionsByScheduleTestRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<GetQuestionsByScheduleTestRequestHandler> logger)
    : IGetQuestionsByScheduleTestRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<Question>>> HandleAsync(GetQuestionsByScheduleTestRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = Validate(request);
            if (validationResult.IsFailed)
                return validationResult;
            
            var questions = await dbContext.ScheduleTests
                .Where(st => st.Id == request.ScheduleTestId)
                .Include(st => st.Test!)
                .ThenInclude(t => t.TestQuestions)
                .ThenInclude(q => q.Question!)
                .SelectMany(st => st.Test!.TestQuestions.Select(tq => tq.Question!))
                .Include(q => q.CorrectAnswer)
                .ToListAsync(cancellationToken);

            return Result.Success(questions);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Questions has not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<IReadOnlyCollection<Question>>(ex, "An error occurred while retrieving the questions");
        }
    }

    private static IValidationFinalResult<IReadOnlyCollection<Question>> Validate(GetQuestionsByScheduleTestRequest request)
    {
        var validationResult = ValidationResult.Create<IReadOnlyCollection<Question>>();
        
        if (request.ScheduleTestId == default)
            validationResult.Fail(nameof(request.ScheduleTestId), "ScheduleTestId is required");
        
        return validationResult;
    }
}