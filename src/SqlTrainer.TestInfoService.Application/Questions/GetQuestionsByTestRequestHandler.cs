﻿using Application;
using Application.Mediator.Extensions;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Questions;

public sealed record GetQuestionsByTestRequest(
    Guid TestId, 
    string SearchTerm, 
    string OrderBy, 
    SortDirection OrderByDirection, 
    int Page, 
    int PageSize) 
    : GetManyRequest(SearchTerm, OrderBy, OrderByDirection, Page, PageSize);

public interface IGetQuestionsByTestRequestHandler : IRequestHandler<GetQuestionsByTestRequest, IReadOnlyCollection<Question>>;

public sealed class GetQuestionsByTestRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<GetQuestionsByTestRequestHandler> logger)
    : IGetQuestionsByTestRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<Question>>> HandleAsync(GetQuestionsByTestRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = Validate(request);
            if (validationResult.IsFailed)
                return validationResult;

            var questions = await dbContext.TestQuestions
                .Include(tq => tq.Question!)
                .ThenInclude(q => q.CorrectAnswer)
                .Where(tq => tq.TestId == request.TestId)
                .Select(tq => tq.Question!)
                .SortAndPaginate(request)
                .ToListAsync(cancellationToken);

            return Result.Success(questions);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Questions has not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<IReadOnlyCollection<Question>>(ex, "An error occurred while retrieving questions.");
        }
    }
    
    private static IValidationFinalResult<IReadOnlyCollection<Question>> Validate(GetQuestionsByTestRequest request)
    {
        var validationResult = ValidationResult.Create<IReadOnlyCollection<Question>>();
        
        if (request.TestId == default)
            validationResult.Fail(nameof(request.TestId), "TestId is required");
        
        return validationResult;
    }
}