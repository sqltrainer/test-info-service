﻿using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.DAL;

namespace SqlTrainer.TestInfoService.Application.Questions;

public sealed record UpdateQuestionRequest(
    Guid Id,
    string Body,
    int Complexity,
    Guid TopicId,
    Guid DatabaseId,
    string CorrectAnswerBody) 
    : IRequest;

public interface IUpdateQuestionRequestHandler : IRequestHandler<UpdateQuestionRequest>;

public sealed class UpdateQuestionRequestHandler(
    ITestInfoDbContext dbContext, 
    ILogger<UpdateQuestionRequestHandler> logger) 
    : IUpdateQuestionRequestHandler
{
    public async Task<IFinalResult> HandleAsync(UpdateQuestionRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await ValidateAsync(request, cancellationToken);
            if (validationResult.IsFailed)
                return validationResult;
            
            var existingQuestion = await dbContext.Questions
                .Include(q => q.CorrectAnswer)
                .FirstOrDefaultAsync(q => q.Id == request.Id, cancellationToken);
            
            if (existingQuestion is null)
                return Result.Failed("Question does not exist");
            
            if (existingQuestion.CorrectAnswer is null)
                return Result.Failed("Correct answer does not exist");
            
            existingQuestion.Body = request.Body;
            existingQuestion.Complexity = request.Complexity;
            existingQuestion.TopicId = request.TopicId;
            existingQuestion.DatabaseId = request.DatabaseId;
            
            existingQuestion.CorrectAnswer.Body = request.CorrectAnswerBody;
            
            await dbContext.SaveChangesAsync(cancellationToken);
            
            return validationResult;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Question has not been updated. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "An error occurred while updating the question");
        }
    }
    
    private async Task<IValidationFinalResult> ValidateAsync(UpdateQuestionRequest request, CancellationToken cancellationToken = default)
    {
        var validationResult = ValidationResult.Create();
        
        if (request.Id == default)
            validationResult.Fail(nameof(request.Id), "Id is required");
        
        if (string.IsNullOrWhiteSpace(request.Body))
            validationResult.Fail(nameof(request.Body), "Body is required");
        
        if (request.Complexity < 1)
            validationResult.Fail(nameof(request.Complexity), "Complexity must be greater than 0");
        
        if (string.IsNullOrWhiteSpace(request.CorrectAnswerBody))
            validationResult.Fail(nameof(request.CorrectAnswerBody), "CorrectAnswerBody is required");
        
        if (request.TopicId == default)
            validationResult.Fail(nameof(request.TopicId), "TopicId is required");
        
        if (request.DatabaseId == default)
            validationResult.Fail(nameof(request.DatabaseId), "DatabaseId is required");
        
        if (!await dbContext.Topics.AnyAsync(t => t.Id == request.TopicId, cancellationToken))
            validationResult.Fail(nameof(request.TopicId), "Topic does not exist");
        
        return validationResult;
    }
}