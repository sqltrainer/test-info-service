﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests;

public sealed record AddScheduleTestRequest(
    Guid TestId,
    DateTimeOffset StartAt,
    DateTimeOffset FinishAt,
    IReadOnlyCollection<Guid> UserIds) : IRequest;

public interface IAddScheduleTestRequestHandler : IRequestHandler<AddScheduleTestRequest, Guid>;

public sealed class AddScheduleTestRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<AddScheduleTestRequestHandler> logger)
    : IAddScheduleTestRequestHandler
{
    public async Task<IFinalResult<Guid>> HandleAsync(AddScheduleTestRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await ValidateAsync(request, cancellationToken);
            if (validationResult.IsFailed)
                return validationResult;
            
            var scheduleTest = new ScheduleTest
            {
                TestId = request.TestId,
                StartAt = request.StartAt,
                FinishAt = request.FinishAt
            };
            
            scheduleTest.WithUserScheduleTests(request.UserIds
                .Select(userId => new UserScheduleTest(scheduleTest.Id, userId)).ToArray());
            
            await dbContext.ScheduleTests.AddAsync(scheduleTest, cancellationToken);
            await dbContext.SaveChangesAsync(cancellationToken);
            
            validationResult.Complete(scheduleTest.Id);

            return validationResult;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Schedule test has not been added. Request: {Request}", request);
            return UnhandledFailedResult.Create<Guid>(ex, "An error occurred while adding schedule test.");
        }
    }

    private async Task<ValidationResult<Guid>> ValidateAsync(AddScheduleTestRequest request, CancellationToken cancellationToken)
    {
        var validationResult = ValidationResult.Create<Guid>();
        
        if (request.TestId == default)
            validationResult.Fail(nameof(request.TestId), "TestId is required");
        
        if (!await dbContext.Tests.AnyAsync(t => t.Id == request.TestId, cancellationToken))
            validationResult.Fail(nameof(request.TestId), "Test does not exist");
        
        if (request.UserIds.Count == 0)
            validationResult.Fail(nameof(request.UserIds), "At least one user is required");
        
        if (request.FinishAt <= DateTime.UtcNow)
            validationResult.Fail(nameof(request.FinishAt), "FinishAt must be greater than current date");
        
        if (request.FinishAt <= request.StartAt)
            validationResult.Fail(nameof(request.StartAt), "FinishAt must be greater than StartAt");
        
        return validationResult;
    }
}