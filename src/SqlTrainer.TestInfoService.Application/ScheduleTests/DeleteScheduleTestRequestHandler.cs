﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests;

public sealed record DeleteScheduleTestRequest(Guid Id) : IRequest;

public interface IDeleteScheduleTestRequestHandler : IRequestHandler<DeleteScheduleTestRequest>;

public sealed class DeleteScheduleTestRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<DeleteScheduleTestRequestHandler> logger)
    : IDeleteScheduleTestRequestHandler
{
    public async Task<IFinalResult> HandleAsync(DeleteScheduleTestRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var scheduleTest = await dbContext.ScheduleTests.FirstOrDefaultAsync(st => st.Id == request.Id, cancellationToken);
            if (scheduleTest is null)
                return Result.Failed("Schedule test does not exist.");

            dbContext.ScheduleTests.Remove(scheduleTest);
            await dbContext.SaveChangesAsync(cancellationToken);

            return Result.Success();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Schedule test has not been deleted. Request: {Request}", request);
            return Result.Failed("An error occurred while deleting schedule test.");
        }
    }
}