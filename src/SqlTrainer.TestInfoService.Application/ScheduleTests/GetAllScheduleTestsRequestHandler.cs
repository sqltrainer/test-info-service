﻿using Application;
using Application.Mediator.Extensions;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests;

public sealed record GetAllScheduleTestsRequest(
    string SearchTerm,
    string OrderBy,
    SortDirection OrderByDirection,
    int Page,
    int PageSize)
    : GetManyRequest(SearchTerm, OrderBy, OrderByDirection, Page, PageSize);

public interface IGetAllScheduleTestsRequestHandler : IRequestHandler<GetAllScheduleTestsRequest, IReadOnlyCollection<ScheduleTest>>;

public sealed class GetAllScheduleTestsRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<GetAllScheduleTestsRequestHandler> logger)
    : IGetAllScheduleTestsRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<ScheduleTest>>> HandleAsync(GetAllScheduleTestsRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var query = dbContext.ScheduleTests.Include(st => st.Test).AsNoTracking();
            
            if (!string.IsNullOrWhiteSpace(request.SearchTerm))
                query = query.Where(st => st.Test!.Name.ToLower().Contains(request.SearchTerm.ToLower()));
            
            var scheduleTests = await query
                .Include(st => st.UserScheduleTests)
                .SortAndPaginate(request)
                .ToListAsync(cancellationToken);
            
            return Result.Success(scheduleTests);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Schedule tests has not been retrieved. Request: {Request}", request);
            return Result.Failed<IReadOnlyCollection<ScheduleTest>>("An error occurred while retrieving schedule tests.");
        }
    }
}