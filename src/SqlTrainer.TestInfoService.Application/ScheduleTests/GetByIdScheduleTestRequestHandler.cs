﻿using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests;

public sealed record GetByIdScheduleTestRequest(Guid Id) : IRequest;

public interface IGetByIdScheduleTestRequestHandler : IRequestHandler<GetByIdScheduleTestRequest, ScheduleTest>;

public sealed class GetByIdScheduleTestRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<GetByIdScheduleTestRequestHandler> logger)
    : IGetByIdScheduleTestRequestHandler
{
    public async Task<IFinalResult<ScheduleTest>> HandleAsync(GetByIdScheduleTestRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var scheduleTest = await dbContext.ScheduleTests.FirstOrDefaultAsync(st => st.Id == request.Id, cancellationToken);

            return scheduleTest is null ? Result.NotFound<ScheduleTest>() : Result.Success(scheduleTest);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Schedule test has not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<ScheduleTest>(ex, "An error occurred while getting schedule test.");
        }
    }
}