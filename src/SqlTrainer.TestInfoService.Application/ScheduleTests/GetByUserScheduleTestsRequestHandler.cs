﻿using Application;
using Application.Mediator.Extensions;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests;

public sealed record GetByUserScheduleTestsRequest(
    Guid UserId, 
    string SearchTerm, 
    string OrderBy, 
    SortDirection OrderByDirection, 
    int Page, 
    int PageSize) 
    : GetManyRequest(SearchTerm, OrderBy, OrderByDirection, Page, PageSize);

public interface IGetByUserScheduleTestsRequestHandler : IRequestHandler<GetByUserScheduleTestsRequest, IReadOnlyCollection<ScheduleTest>>;

public sealed class GetByUserScheduleTestsRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<GetByUserScheduleTestsRequestHandler> logger)
    : IGetByUserScheduleTestsRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<ScheduleTest>>> HandleAsync(GetByUserScheduleTestsRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var query = dbContext.ScheduleTests
                .Include(st => st.Test)
                .Include(st => st.UserScheduleTests)
                .Where(st => st.UserScheduleTests.Any(ust => ust.UserId == request.UserId))
                .AsNoTracking();


            if (!string.IsNullOrWhiteSpace(request.SearchTerm))
                query = query.Where(st => st.Test!.Name.ToLower().Contains(request.SearchTerm.ToLower()));

            var scheduleTests = await query.SortAndPaginate(request).ToListAsync(cancellationToken);

            return Result.Success(scheduleTests);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Schedule tests have been not retrieved. Request: {Request}", request);
            return Result.Failed<IReadOnlyCollection<ScheduleTest>>("An error occurred while retrieving schedule tests.");
        }
    }
}