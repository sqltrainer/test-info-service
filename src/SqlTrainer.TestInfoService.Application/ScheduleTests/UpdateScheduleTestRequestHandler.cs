﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests;

public sealed record UpdateScheduleTestRequest(
    Guid Id,
    Guid TestId,
    DateTimeOffset StartAt,
    DateTimeOffset FinishAt,
    IReadOnlyCollection<Guid> UserIds) 
    : IRequest;

public interface IUpdateScheduleTestRequestHandler : IRequestHandler<UpdateScheduleTestRequest>;

public sealed class UpdateScheduleTestRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<UpdateScheduleTestRequestHandler> logger)
    : IUpdateScheduleTestRequestHandler
{
    public async Task<IFinalResult> HandleAsync(UpdateScheduleTestRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await ValidateAsync(request, cancellationToken);
            if (validationResult.IsFailed)
                return validationResult;

            var scheduleTest = new ScheduleTest(request.Id)
            {
                TestId = request.TestId,
                StartAt = request.StartAt,
                FinishAt = request.FinishAt
            };
            
            var userScheduleTests = await dbContext.UserScheduleTests
                .Where(ust => ust.ScheduleTestId == scheduleTest.Id)
                .ToListAsync(cancellationToken);
            
            dbContext.UserScheduleTests.RemoveRange(userScheduleTests);
            
            dbContext.UserScheduleTests.AddRange(request.UserIds
                .Select(userId => new UserScheduleTest(scheduleTest.Id, userId)).ToArray());

            dbContext.ScheduleTests.Update(scheduleTest);
            await dbContext.SaveChangesAsync(cancellationToken);

            return validationResult;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Schedule test has not been updated. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "An error occurred while updating schedule test.");
        }
    }

    private async Task<ValidationResult> ValidateAsync(UpdateScheduleTestRequest request, CancellationToken cancellationToken = default)
    {
        var validationResult = ValidationResult.Create();
        
        if (request.TestId == default)
            validationResult.Fail(nameof(request.TestId), "TestId is required");
        
        if (!await dbContext.Tests.AnyAsync(t => t.Id == request.TestId, cancellationToken))
            validationResult.Fail(nameof(request.TestId), "Test does not exist");
        
        if (request.UserIds.Count == 0)
            validationResult.Fail(nameof(request.UserIds), "At least one user is required");
        
        if (request.FinishAt <= DateTime.UtcNow)
            validationResult.Fail(nameof(request.FinishAt), "FinishAt must be greater than current date");
        
        if (request.FinishAt <= request.StartAt)
            validationResult.Fail(nameof(request.FinishAt), "FinishAt must be greater than StartAt");

        return validationResult;
    }
}