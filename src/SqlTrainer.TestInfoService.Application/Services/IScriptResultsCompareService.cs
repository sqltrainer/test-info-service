﻿using DatabaseHelper.ScriptResults;

namespace SqlTrainer.TestInfoService.Application.Services;

public interface IScriptResultsCompareService
{
    double Compare(ScriptResult actual, ScriptResult expected, double maxMark);
}