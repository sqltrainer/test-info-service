﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Tests;

public sealed record AddTestRequest(
    string Name, 
    DateTimeOffset CreatedAt, 
    IReadOnlyCollection<(Guid QuestionId, double MaxMark)> TestQuestionsData) 
    : IRequest;

public interface IAddTestRequestHandler : IRequestHandler<AddTestRequest, Guid>;

public sealed class AddTestRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<AddTestRequestHandler> logger)
    : IAddTestRequestHandler
{
    public async Task<IFinalResult<Guid>> HandleAsync(AddTestRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await ValidateAsync(request, cancellationToken);
            if (validationResult.IsFailed)
                return validationResult;
            
            var test = new Test { Name = request.Name, CreatedAt = request.CreatedAt };
            dbContext.Tests.Add(test);
            
            await dbContext.SaveChangesAsync(cancellationToken);

            test.AddTestQuestions(request.TestQuestionsData
                .Select(tq => new TestQuestion(test.Id, tq.QuestionId)
                {
                    MaxMark = tq.MaxMark
                }));
            
            await dbContext.SaveChangesAsync(cancellationToken);
            
            validationResult.Complete(test.Id);
            
            return validationResult;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Test has not been added. Request: {Request}", request);
            return UnhandledFailedResult.Create<Guid>(ex, "An error occurred while adding the test");
        }
    }

    private async Task<ValidationResult<Guid>> ValidateAsync(AddTestRequest request, CancellationToken cancellationToken = default)
    {
        var validationResult = ValidationResult.Create<Guid>();
        
        if (string.IsNullOrWhiteSpace(request.Name))
            validationResult.Fail(nameof(request.Name), "Name is required");

        if (DateTimeOffset.UtcNow < request.CreatedAt)
            validationResult.Fail(nameof(request.CreatedAt), "CreatedAt is required");

        if (request.TestQuestionsData.Count == 0)
            validationResult.Fail(nameof(request.TestQuestionsData), "TestQuestionsData is required");
        
        var questionIds = request.TestQuestionsData.Select(x => x.QuestionId).ToList();
        var questionsCount = await dbContext.Questions
            .Where(tq => questionIds.Contains(tq.Id))
            .CountAsync(cancellationToken);
        
        if (questionsCount != request.TestQuestionsData.Count)
            validationResult.Fail(nameof(request.Name), "Some questions are not found");

        foreach (var _ in request.TestQuestionsData.Where(tq => tq.MaxMark <= 0.0))
            validationResult.Fail(nameof(TestQuestion.MaxMark), "MaxMark must be greater than 0");
        
        return validationResult;
    }
}