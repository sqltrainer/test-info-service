﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;

namespace SqlTrainer.TestInfoService.Application.Tests;

public sealed record DeleteTestRequest(Guid Id) : IRequest;

public interface IDeleteTestRequestHandler : IRequestHandler<DeleteTestRequest>;

public sealed class DeleteTestRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<DeleteTestRequestHandler> logger)
    : IDeleteTestRequestHandler
{
    public async Task<IFinalResult> HandleAsync(DeleteTestRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var test = await dbContext.Tests.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken: cancellationToken);
            if (test is null)
                return Result.Failed("Test does not exist.");

            dbContext.Tests.Remove(test);

            await dbContext.SaveChangesAsync(cancellationToken);

            return Result.Success();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Test has not been deleted. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "An error occurred while deleting the test");
        }
    }
}