﻿using Application;
using Application.Mediator.Extensions;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Tests;

public sealed record GetAllTestsRequest(
    string SearchTerm, 
    string OrderBy, 
    SortDirection OrderByDirection, 
    int Page, 
    int PageSize)
    : GetManyRequest(SearchTerm, OrderBy, OrderByDirection, Page, PageSize);

public interface IGetAllTestsRequestHandler : IRequestHandler<GetAllTestsRequest, IReadOnlyCollection<Test>>;

public sealed class GetAllTestsRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<GetAllTestsRequestHandler> logger)
    : IGetAllTestsRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<Test>>> HandleAsync(GetAllTestsRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var query = dbContext.Tests.AsNoTracking();
            
            if (!string.IsNullOrWhiteSpace(request.SearchTerm))
                query = query.Where(q => q.Name.ToLower().Contains(request.SearchTerm.ToLower()));
            
            var tests = await query.SortAndPaginate(request).ToListAsync(cancellationToken);
            
            return Result.Success(tests);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Tests have not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<IReadOnlyCollection<Test>>(ex, "An error occurred while retrieving the tests");
        }
    }
}