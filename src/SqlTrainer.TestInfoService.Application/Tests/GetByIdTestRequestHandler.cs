﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Tests;

public sealed record GetByIdTestRequest(Guid Id) : IRequest;

public interface IGetByIdTestRequestHandler : IRequestHandler<GetByIdTestRequest, Test>;

public sealed class GetByIdTestRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<GetByIdTestRequestHandler> logger)
    : IGetByIdTestRequestHandler
{
    public async Task<IFinalResult<Test>> HandleAsync(GetByIdTestRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var test = await dbContext.Tests.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken: cancellationToken);
            
            return test is null ? Result.NotFound<Test>() : Result.Success(test);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Test has not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<Test>(ex, "An error occurred while retrieving the test");
        }
    }
}