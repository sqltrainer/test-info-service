﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Tests;

public sealed record UpdateTestRequest(
    Guid Id, 
    string Name, 
    IReadOnlyCollection<(Guid QuestionId, double MaxMark)> TestQuestionsData) 
    : IRequest;

public interface IUpdateTestRequestHandler : IRequestHandler<UpdateTestRequest>;

public sealed class UpdateTestRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<UpdateTestRequestHandler> logger)
    : IUpdateTestRequestHandler
{
    public async Task<IFinalResult> HandleAsync(UpdateTestRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await ValidateAsync(request, cancellationToken);
            if (validationResult.IsFailed)
                return validationResult;
            
            var test = validationResult.Value;
            
            test.Name = request.Name;
            
            dbContext.TestQuestions.RemoveRange(test.TestQuestions);

            test.ClearTestQuestions();
            
            test.AddTestQuestions(request.TestQuestionsData
                .Select(tq => new TestQuestion(request.Id, tq.QuestionId)
                {
                    MaxMark = tq.MaxMark
                }));
            
            await dbContext.SaveChangesAsync(cancellationToken);

            return validationResult;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Test has not been updated. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "An error occurred while updating the test");
        }
    }

    private async Task<ValidationResult<Test>> ValidateAsync(UpdateTestRequest request, CancellationToken cancellationToken = default)
    {
        var validationResult = ValidationResult.Create<Test>();
        
        if (request.Id == default)
            validationResult.Fail(nameof(request.Id), "Id is required");
        
        if (string.IsNullOrWhiteSpace(request.Name))
            validationResult.Fail(nameof(request.Name), "Name is required");

        if (request.TestQuestionsData.Count == 0)
            validationResult.Fail(nameof(request.TestQuestionsData), "TestQuestionsData is required");
        
        var questionIds = request.TestQuestionsData.Select(x => x.QuestionId).ToList();
        var questionsCount = await dbContext.TestQuestions
            .Where(tq => questionIds.Contains(tq.QuestionId))
            .CountAsync(cancellationToken);
        
        if (questionsCount != request.TestQuestionsData.Count)
            validationResult.Fail(nameof(request.TestQuestionsData), "Some questions are not found");

        foreach (var (_, maxMark) in request.TestQuestionsData)
            if (maxMark <= 0.0)
                validationResult.Fail(nameof(TestQuestion.MaxMark), "MaxMark must be greater than 0");

        var test = await dbContext.Tests
            .Include(t => t.TestQuestions)
            .FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken: cancellationToken);
        
        if (test is null)
            validationResult.Fail(nameof(request.Id), "Test with such id does not exist");
        else
            validationResult.Complete(test);
        
        return validationResult;
    }
}