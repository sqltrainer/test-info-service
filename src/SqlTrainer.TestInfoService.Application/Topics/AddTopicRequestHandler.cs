﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Topics;

public sealed record AddTopicRequest(string Name) : IRequest;

public interface IAddTopicRequestHandler : IRequestHandler<AddTopicRequest, Guid>;

public sealed class AddTopicRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<AddTopicRequestHandler> logger)
    : IAddTopicRequestHandler
{
    public async Task<IFinalResult<Guid>> HandleAsync(AddTopicRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var result = Validate(request);
            if (result.IsFailed)
                return result;
            
            
            var topic = new Topic { Name = request.Name };
            dbContext.Topics.Add(topic);
            
            await dbContext.SaveChangesAsync(cancellationToken);
            
            result.Complete(topic.Id);
            
            return result;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Topic has not been added. Request: {Request}", request);
            return UnhandledFailedResult.Create<Guid>(ex, "An error occurred while adding the topic");
        }
    }
    
    private static ValidationResult<Guid> Validate(AddTopicRequest request)
    {
        var validationResult = ValidationResult.Create<Guid>();
        
        if (string.IsNullOrWhiteSpace(request.Name))
            validationResult.Fail(nameof(request.Name), "Name is required.");

        return validationResult;
    }
}