﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;

namespace SqlTrainer.TestInfoService.Application.Topics;

public sealed record DeleteTopicRequest(Guid Id) : IRequest;

public interface IDeleteTopicRequestHandler : IRequestHandler<DeleteTopicRequest>;

public sealed class DeleteTopicRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<DeleteTopicRequestHandler> logger)
    : IDeleteTopicRequestHandler
{
    public async Task<IFinalResult> HandleAsync(DeleteTopicRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var topic = await dbContext.Topics.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken: cancellationToken);
            if (topic is null)
                return Result.Failed("Topic does not exist.");

            dbContext.Topics.Remove(topic);

            await dbContext.SaveChangesAsync(cancellationToken);

            return Result.Success();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Topic has not been deleted. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "An error occurred while deleting the topic");
        }
    }
}