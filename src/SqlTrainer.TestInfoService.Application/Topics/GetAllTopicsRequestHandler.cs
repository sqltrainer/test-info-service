﻿using Application;
using Application.Mediator.Extensions;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Topics;

public sealed record GetAllTopicsRequest(
    string SearchTerm, 
    string OrderBy, 
    SortDirection OrderByDirection, 
    int Page, 
    int PageSize)
    : GetManyRequest(SearchTerm, OrderBy, OrderByDirection, Page, PageSize);

public interface IGetAllTopicsRequestHandler : IRequestHandler<GetAllTopicsRequest, IReadOnlyCollection<Topic>>;

public sealed class GetAllTopicsRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<GetAllTopicsRequestHandler> logger)
    : IGetAllTopicsRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<Topic>>> HandleAsync(GetAllTopicsRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var query = dbContext.Topics.AsNoTracking();
            
            if (!string.IsNullOrWhiteSpace(request.SearchTerm))
                query = query.Where(q => q.Name.ToLower().Contains(request.SearchTerm.ToLower()));
            
            var topics = await query.SortAndPaginate(request).ToListAsync(cancellationToken);
            
            return Result.Success(topics);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Topics have not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<IReadOnlyCollection<Topic>>(ex, "An error occurred while retrieving the topics");
        }
    }
}