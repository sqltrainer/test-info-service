﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Topics;

public sealed record GetByIdTopicRequest(Guid Id) : IRequest;

public interface IGetByIdTopicRequestHandler : IRequestHandler<GetByIdTopicRequest, Topic>;

public sealed class GetByIdTopicRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<GetByIdTopicRequestHandler> logger)
    : IGetByIdTopicRequestHandler
{
    public async Task<IFinalResult<Topic>> HandleAsync(GetByIdTopicRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var topic = await dbContext.Topics.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken: cancellationToken);
            
            return topic is null ? Result.NotFound<Topic>() : Result.Success(topic);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Topic has not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<Topic>(ex, "An error occurred while retrieving the topic");
        }
    }
}