﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Topics;

public sealed record UpdateTopicRequest(Guid Id, string Name) : IRequest;

public interface IUpdateTopicRequestHandler : IRequestHandler<UpdateTopicRequest>;

public sealed class UpdateTopicRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<UpdateTopicRequestHandler> logger)
    : IUpdateTopicRequestHandler
{
    public async Task<IFinalResult> HandleAsync(UpdateTopicRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = Validate(request);
            if (validationResult.IsFailed)
                return validationResult;
            
            var topic = new Topic(request.Id) { Name = request.Name };

            dbContext.Topics.Update(topic);
            
            await dbContext.SaveChangesAsync(cancellationToken);
            
            return validationResult;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Topic has not been updated. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "An error occurred while updating the topic");
        }
    }

    private static ValidationResult Validate(UpdateTopicRequest request)
    {
        var validationResult = ValidationResult.Create();
        
        if (request.Id == Guid.Empty)
            validationResult.Fail(nameof(request.Id), "Id is required.");
        
        if (string.IsNullOrWhiteSpace(request.Name))
            validationResult.Fail(nameof(request.Name), "Name is required.");
        
        return validationResult;
    }
}