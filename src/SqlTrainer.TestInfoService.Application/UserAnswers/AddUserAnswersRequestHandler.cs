﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using DatabaseHelper.ScriptResults;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Application.Services;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.UserAnswers;

public record QuestionBody(Guid QuestionId, string? Body);

public sealed record QuestionAnswerComparingModel(Guid QuestionId, ScriptResult Actual, ScriptResult Expected, double MaxMark);

public record DatabaseQuestionBody(Guid DatabaseId, Guid QuestionId, string? Body) : QuestionBody(QuestionId, Body);

public sealed record AddUserAnswersRequest(
    Guid ScheduleTestId,
    Guid UserId,
    DateTimeOffset AnsweredAt, 
    IReadOnlyCollection<QuestionBody> QuestionBodies)
    : IRequest;

public interface IAddUserAnswersRequestHandler : IRequestHandler<AddUserAnswersRequest>;


public sealed class AddUserAnswersRequestHandler(
    ITestInfoDbContext dbContext,
    IScriptService scriptService,
    IScriptFormattingService scriptFormattingService,
    IScriptResultsCompareService scriptResultsCompareService,
    ILogger<AddUserAnswersRequestHandler> logger)
    : IAddUserAnswersRequestHandler
{
    public async Task<IFinalResult> HandleAsync(AddUserAnswersRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await ValidateAsync(request, cancellationToken);
            if (validationResult.IsFailed)
                return validationResult;
            
            var userAnswers = await HandleRequestAsync(request, validationResult.Value, cancellationToken);
            
            await dbContext.UserAnswers.AddRangeAsync(userAnswers, cancellationToken);
            await dbContext.SaveChangesAsync(cancellationToken);
            
            return validationResult;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "User answers has not been added. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "An error occurred while adding the user answers");
        }
    }
    
    // ToDo: refactor adding of user answers
    private async Task<IReadOnlyCollection<UserAnswer>> HandleRequestAsync(AddUserAnswersRequest request, TestQuestionAnswers testQuestionAnswers, CancellationToken cancellationToken = default)
    {
        var (questions, correctAnswers, testQuestions) = testQuestionAnswers;
        var userAnswersToSave = new List<UserAnswer>();

        // Convert QuestionBody to DatabaseQuestionBody
        var initialUserAnswerBodies = request.QuestionBodies
            .Select(qb => new DatabaseQuestionBody(questions.First(q => q.Id == qb.QuestionId).DatabaseId, qb.QuestionId, scriptFormattingService.Clear(qb.Body)))
            .ToArray();
        
        // Filter answers that could not be executed and format them
        var userAnswerBodiesToSave = initialUserAnswerBodies.Where(qb => string.IsNullOrWhiteSpace(qb.Body));
        userAnswersToSave.AddRange(userAnswerBodiesToSave.Select(uab =>
            new UserAnswer(request.ScheduleTestId, uab.QuestionId, request.UserId)
            {
                Body = uab.Body ?? string.Empty,
                ProgramMark = 0.0,
                AnsweredAt = request.AnsweredAt
            }));
        
        // Get empty answers to save immediately
        var userAnswerBodies = initialUserAnswerBodies
            .Where(qb => !string.IsNullOrWhiteSpace(qb.Body))
            .OrderBy(qb => qb.QuestionId)
            .ToList();
        foreach (var userAnswerBody in userAnswerBodies)
            logger.LogInformation("User answer for question {QuestionId} is {Body}", userAnswerBody.QuestionId, userAnswerBody.Body);
        
        // Filter correct answers according to user answers and format them
        var correctAnswerBodies = correctAnswers
            .Select(ca => new DatabaseQuestionBody(ca.Question!.DatabaseId, ca.QuestionId, scriptFormattingService.Clear(ca.Body)))
            .Where(ca => userAnswerBodies.Select(uab => uab.QuestionId).Contains(ca.QuestionId) && !string.IsNullOrWhiteSpace(ca.Body))
            .OrderBy(qb => qb.QuestionId)
            .ToList();
        foreach (var correctAnswerBody in correctAnswerBodies)
			logger.LogInformation("Correct answer for question {QuestionId} is {Body}", correctAnswerBody.QuestionId, correctAnswerBody.Body);
        
        if (correctAnswerBodies.Count != correctAnswers.Count)
        {
            logger.LogWarning("Not all correct answers could be executed");
            userAnswersToSave.AddRange(userAnswerBodies.Select(uab => 
                new UserAnswer(request.ScheduleTestId, uab.QuestionId, request.UserId)
                {
                    Body = uab.Body ?? string.Empty,
                    ProgramMark = 0.0,
                    AnsweredAt = request.AnsweredAt
                }));

            return userAnswersToSave;
        }
        
        for (var i = 0; i < userAnswerBodies.Count; i++)
        {
            var userAnswerBody = userAnswerBodies[i];
            var correctAnswerBody = correctAnswerBodies[i];
            
            if (!userAnswerBody.Body!.Equals(correctAnswerBody.Body))
                continue;
            
            // Remove all correct answers to not execute them
            var testQuestion = testQuestions.First(tq => tq.QuestionId == userAnswerBody.QuestionId);
            userAnswersToSave.Add(new UserAnswer(request.ScheduleTestId, userAnswerBody.QuestionId, request.UserId)
            {
                Body = userAnswerBody.Body,
                ProgramMark = testQuestion.MaxMark,
                AnsweredAt = request.AnsweredAt
            });
            
            userAnswerBodies.Remove(userAnswerBody);
            correctAnswerBodies.Remove(correctAnswerBody);
            i--;
        }
        
        var allAnswerBodies = userAnswerBodies.Concat(correctAnswerBodies).ToList();
        var comparingModels = new List<QuestionAnswerComparingModel>();
        foreach (var answersByDatabase in allAnswerBodies.GroupBy(answer => answer.DatabaseId))
        {
            logger.LogInformation("Executing scripts for database {DatabaseId}", answersByDatabase.Key);
            var executeResult = await scriptService.ExecuteScriptsAsync(answersByDatabase
                .Select(answer => answer.Body!), answersByDatabase.Key, cancellationToken);

            var results = new List<ScriptResult>();
            if (!executeResult.IsSucceeded)
            {
                // ToDo: think about bad scenario
                continue;
            }
            
            logger.LogInformation("Scripts for database {DatabaseId} have been executed", answersByDatabase.Key);
            results.AddRange(executeResult.Value);
            foreach (var result in results)
                logger.LogInformation("Result for script {Script} is {Result}", result.Script, result.ToFinalJson());

            var correctAnswersByDatabase = correctAnswerBodies.Where(dqb => dqb.DatabaseId == answersByDatabase.Key).Select(dqb => dqb.Body).ToList();
            var userAnswersByDatabase = userAnswerBodies.Where(dqb => dqb.DatabaseId == answersByDatabase.Key).Select(dqb => dqb.Body).ToList();
            foreach (var answersByQuestion in answersByDatabase.GroupBy(dqb => dqb.QuestionId))
            {
                logger.LogInformation("Started comparing answers for question {QuestionId}", answersByQuestion.Key);
                var testQuestion = testQuestions.First(tq => tq.QuestionId == answersByQuestion.Key);
                
                logger.LogInformation("Correct answers for question {QuestionId} are {CorrectAnswers}", answersByQuestion.Key, string.Join(", ", correctAnswersByDatabase));
                var correctAnswerBody = answersByQuestion.First(dqb => correctAnswersByDatabase.Contains(dqb.Body)).Body;
                logger.LogInformation("User answers for question {QuestionId} are {UserAnswers}", answersByQuestion.Key, string.Join(", ", userAnswersByDatabase));
                var userAnswerBody = answersByQuestion.First(dqb => userAnswersByDatabase.Contains(dqb.Body)).Body;

                logger.LogInformation("Searching for correct answer for question {QuestionId} with body {Body}", answersByQuestion.Key, correctAnswerBody);
                var correctAnswer = results.First(res => res.Script.Equals(correctAnswerBody));
                logger.LogInformation("Searching for user answer for question {QuestionId} with body {Body}", answersByQuestion.Key, userAnswerBody);
                var userAnswer = results.First(res => res.Script.Equals(userAnswerBody));
                
                comparingModels.Add(new QuestionAnswerComparingModel(answersByQuestion.Key, userAnswer, correctAnswer, testQuestion.MaxMark));
                logger.LogInformation("Answers for question {QuestionId} have been compared", answersByQuestion.Key);
            }
        }

        userAnswersToSave.AddRange(comparingModels.Select(comparingModel => 
            new UserAnswer(request.ScheduleTestId, comparingModel.QuestionId, request.UserId)
            {
                Body = request.QuestionBodies.First(qb => qb.QuestionId == comparingModel.QuestionId).Body!, 
                ProgramMark = scriptResultsCompareService.Compare(comparingModel.Actual, comparingModel.Expected, comparingModel.MaxMark), 
                AnsweredAt = request.AnsweredAt
            }));
        
        return userAnswersToSave;
    }
    
    private sealed record TestQuestionAnswers(
        IReadOnlyCollection<Question> Questions, 
        IReadOnlyCollection<CorrectAnswer> CorrectAnswers, 
        IReadOnlyCollection<TestQuestion> TestQuestions);
    
    private async Task<IValidationFinalResult<TestQuestionAnswers>> ValidateAsync(AddUserAnswersRequest request, CancellationToken cancellationToken = default)
    {
        var validationResult = ValidationResult.Create<TestQuestionAnswers>();
        
        var questions = await dbContext.Questions
            .AsNoTracking()
            .Where(q => request.QuestionBodies.Select(qb => qb.QuestionId).Contains(q.Id))
            .ToListAsync(cancellationToken);
        
        if (questions.Count != request.QuestionBodies.Count)
            validationResult.Fail(nameof(questions), "Not all questions exist");

        var correctAnswers = await dbContext.CorrectAnswers
            .AsNoTracking()
            .Include(ca => ca.Question)
            .Where(ca => questions.Select(q => q.Id).Contains(ca.QuestionId))
            .ToListAsync(cancellationToken);
        
        if (correctAnswers.Count != questions.Count)
            validationResult.Fail(nameof(correctAnswers), "Not all questions have correct answers");
        
        var scheduleTest = await dbContext.ScheduleTests
            .AsNoTracking()
            .FirstOrDefaultAsync(st => st.Id == request.ScheduleTestId, cancellationToken);

        if (scheduleTest is null)
        {
            validationResult.Fail(nameof(scheduleTest), "ScheduleTest does not exist");
            return validationResult;
        }

        var testQuestions = await dbContext.TestQuestions
            .AsNoTracking()
            .Where(tq => tq.TestId == scheduleTest.TestId && questions.Select(q => q.Id).Contains(tq.QuestionId))
            .ToListAsync(cancellationToken);

        if (testQuestions.Count != questions.Count)
            validationResult.Fail(nameof(testQuestions), "Not all questions are in the test");
        
        validationResult.Complete(new TestQuestionAnswers(questions, correctAnswers, testQuestions));
        
        return validationResult;
    }
}