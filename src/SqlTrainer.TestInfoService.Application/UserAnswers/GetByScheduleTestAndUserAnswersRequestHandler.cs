﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.UserAnswers;

public sealed record GetByScheduleTestAndUserAnswersRequest(Guid ScheduleTestId, Guid UserId) : IRequest;

public interface IGetByScheduleTestAndUserAnswersRequestHandler : IRequestHandler<GetByScheduleTestAndUserAnswersRequest, IReadOnlyCollection<UserAnswer>>;

public sealed class GetByScheduleTestAndUserAnswersRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<GetByScheduleTestAndUserAnswersRequestHandler> logger)
    : IGetByScheduleTestAndUserAnswersRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<UserAnswer>>> HandleAsync(GetByScheduleTestAndUserAnswersRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var userAnswers = await dbContext.UserAnswers
                .AsNoTracking()
                .Where(ua => ua.ScheduleTestId == request.ScheduleTestId && ua.UserId == request.UserId)
                .ToListAsync(cancellationToken);
            
            return Result.Success(userAnswers);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "User answers have not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<IReadOnlyCollection<UserAnswer>>(ex, "An error occurred while retrieving the user answers");
        }
    }
}