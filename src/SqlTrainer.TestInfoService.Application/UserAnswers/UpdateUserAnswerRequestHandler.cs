﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.UserAnswers;

public sealed record UpdateUserAnswerRequest(
    Guid ScheduleTestId, 
    Guid UserId, 
    Guid QuestionId, 
    double TeacherMark) 
    : IRequest;


public interface IUpdateUserAnswerRequestHandler : IRequestHandler<UpdateUserAnswerRequest>;

public sealed class UpdateUserAnswerRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<UpdateUserAnswerRequestHandler> logger)
    : IUpdateUserAnswerRequestHandler
{
    public async Task<IFinalResult> HandleAsync(UpdateUserAnswerRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await ValidateAsync(request, cancellationToken);
            if (validationResult.IsFailed)
                return validationResult;
            
            var existingUserAnswer = await dbContext.UserAnswers
                .FirstOrDefaultAsync(ua => ua.ScheduleTestId == request.ScheduleTestId && ua.QuestionId == request.QuestionId && ua.UserId == request.UserId, cancellationToken);
            
            if (existingUserAnswer is null)
                return Result.Failed("User answer does not exist");
            
            existingUserAnswer.TeacherMark = request.TeacherMark;
            
            await dbContext.SaveChangesAsync(cancellationToken);
            
            return validationResult;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "User answer has not been updated. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "An error occurred while updating the user answer");
        }
    }
    
    private async Task<IValidationFinalResult> ValidateAsync(UpdateUserAnswerRequest request, CancellationToken cancellationToken = default)
    {
        var validationResult = ValidationResult.Create();
        
        ScheduleTest? scheduleTest = null;
        if (request.ScheduleTestId != default)
        {
            scheduleTest = await dbContext.ScheduleTests
                .AsNoTracking()
                .FirstOrDefaultAsync(st => st.Id == request.ScheduleTestId, cancellationToken);

            if (scheduleTest is null)
                validationResult.Fail(nameof(request.ScheduleTestId), "ScheduleTest does not exist");
        }
        else
        {
            validationResult.Fail(nameof(request.ScheduleTestId), "ScheduleTestId is required");
        }

        if (request.UserId == default)
            validationResult.Fail(nameof(request.UserId), "UserId is required");

        Question? question = null;
        if (request.QuestionId != default)
        {
            question = await dbContext.Questions
                .AsNoTracking()
                .FirstOrDefaultAsync(q => q.Id == request.QuestionId, cancellationToken);
            
            if (question is null)
                validationResult.Fail(nameof(request.QuestionId), "Question does not exist");
        }
        else
        {
            validationResult.Fail(nameof(request.QuestionId), "QuestionId is required");
        }

        if (request.TeacherMark < 0)
            validationResult.Fail(nameof(request.TeacherMark), "Mark must be greater than or equal to 0");
        
        if (scheduleTest is null || question is null)
            return validationResult;
        
        var testQuestion = await dbContext.TestQuestions
            .AsNoTracking()
            .FirstOrDefaultAsync(tq => tq.QuestionId == request.QuestionId && tq.TestId == scheduleTest.TestId, cancellationToken);

        if (testQuestion is null)
            validationResult.Fail(nameof(testQuestion), "TestQuestion does not exist");
        else if (testQuestion.MaxMark < request.TeacherMark)
            validationResult.Fail(nameof(request.TeacherMark), "Mark must be less than or equal to MaxMark");

        return validationResult;
    }
}