﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.TestInfoService.Application.DAL;

namespace SqlTrainer.TestInfoService.Application.UserScheduleTests;

public sealed record UpdateUserScheduleTestRequest(
    Guid ScheduleTestId,
    Guid UserId,
    DateTimeOffset? FinishedAt,
    bool? CheckedByTeacher) 
    : IRequest;

public interface IUpdateUserScheduleTestRequestHandler : IRequestHandler<UpdateUserScheduleTestRequest>;

public sealed class UpdateUserScheduleTestRequestHandler(
    ITestInfoDbContext dbContext,
    ILogger<UpdateUserScheduleTestRequestHandler> logger) 
    : IUpdateUserScheduleTestRequestHandler
{
    public async Task<IFinalResult> HandleAsync(UpdateUserScheduleTestRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await ValidateAsync(request, cancellationToken);
            if (validationResult.IsFailed)
                return validationResult;
            
            var existingUserScheduleTest = await dbContext.UserScheduleTests
                .FirstOrDefaultAsync(ust => ust.ScheduleTestId == request.ScheduleTestId && ust.UserId == request.UserId, cancellationToken);
            
            if (existingUserScheduleTest is null)
                return Result.Failed("User schedule test does not exist");
            
            if (request.FinishedAt.HasValue && !existingUserScheduleTest.FinishedAt.HasValue)
                existingUserScheduleTest.UserFinishedTest(request.FinishedAt.Value);
            
            if (request.CheckedByTeacher.HasValue)
                existingUserScheduleTest.SetTeacherChecked(request.CheckedByTeacher.Value);
            
            await dbContext.SaveChangesAsync(cancellationToken);
            
            return validationResult;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "User schedule test has not been updated. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "An error occurred while updating the user schedule test");
        }
    }
    
    private async Task<IValidationFinalResult> ValidateAsync(UpdateUserScheduleTestRequest request, CancellationToken cancellationToken)
    {
        var validationResult = ValidationResult.Create();
        
        if (request.UserId == default)
            validationResult.Fail(nameof(request.UserId), "UserId is required");
        
        if (request.ScheduleTestId == default)
            validationResult.Fail(nameof(request.UserId), "ScheduleTestId is required");
        
        if (request.FinishedAt.HasValue)
        {
            if (request.FinishedAt.Value == default)
                validationResult.Fail(nameof(request.UserId), "FinishedAt is required, when it is not null");
            
            if (DateTime.UtcNow < request.FinishedAt.Value)
                validationResult.Fail(nameof(request.UserId), "FinishedAt must be less than UTC now");
        }
        
        if (!await dbContext.ScheduleTests.AnyAsync(st => st.Id == request.ScheduleTestId, cancellationToken))
            validationResult.Fail(nameof(request.UserId), $"Schedule test with id {request.ScheduleTestId} does not exist");


        return validationResult;
    }
}