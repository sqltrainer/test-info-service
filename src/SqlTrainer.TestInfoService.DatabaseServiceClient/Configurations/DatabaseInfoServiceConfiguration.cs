﻿namespace SqlTrainer.TestInfoService.DatabaseServiceClient.Configurations;

public sealed class DatabaseInfoServiceConfiguration
{
    public required Uri BaseUri { get; init; }
}