using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlTrainer.TestInfoService.Application.Services;
using SqlTrainer.TestInfoService.DatabaseServiceClient.Configurations;
using SqlTrainer.TestInfoService.DatabaseServiceClient.Services;

namespace SqlTrainer.TestInfoService.DatabaseServiceClient.Extensions;

public static class DependencyInjectionExtensions
{
    public static IServiceCollection AddDatabaseServices(this IServiceCollection services, IConfiguration configuration)
    {
        var databaseServiceUrl = configuration.GetSection("DatabaseInfoService").GetValue<string>("Url")
            ?? throw new ArgumentNullException(nameof(configuration), "DatabaseInfoService:Url is required.");

        return services
            .AddSingleton(new DatabaseInfoServiceConfiguration { BaseUri = new Uri(databaseServiceUrl) })
            .AddTransient<IScriptService, ScriptService>();
    }
}