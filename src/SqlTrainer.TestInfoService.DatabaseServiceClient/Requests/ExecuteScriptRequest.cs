﻿using System.Text.Json.Serialization;

namespace SqlTrainer.TestInfoService.DatabaseServiceClient.Requests;

public sealed class ExecuteScriptRequest
{
    [JsonInclude]
    [JsonPropertyName("databaseId")]
    public required Guid DatabaseId { get; init; }
    
    [JsonInclude]
    [JsonPropertyName("script")]
    public required string Script { get; init; }
}