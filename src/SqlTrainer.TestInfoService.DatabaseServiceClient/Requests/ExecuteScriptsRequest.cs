﻿using System.Text.Json.Serialization;

namespace SqlTrainer.TestInfoService.DatabaseServiceClient.Requests;

public sealed class ExecuteScriptsRequest
{
    [JsonInclude]
    [JsonPropertyName("databaseId")]
    public required Guid DatabaseId { get; init; }
    
    [JsonInclude]
    [JsonPropertyName("scripts")]
    public required IEnumerable<string> Scripts { get; init; }
}