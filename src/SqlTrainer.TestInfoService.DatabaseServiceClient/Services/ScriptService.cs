using System.Net.Http.Json;
using DatabaseHelper.ScriptResults;
using Results;
using SqlTrainer.TestInfoService.Application.Services;
using SqlTrainer.TestInfoService.DatabaseServiceClient.Configurations;
using SqlTrainer.TestInfoService.DatabaseServiceClient.Requests;

namespace SqlTrainer.TestInfoService.DatabaseServiceClient.Services;

public sealed class ScriptService(
    DatabaseInfoServiceConfiguration configuration,
    IHttpClientFactory httpClientFactory)
    : IScriptService
{
    public async Task<IFinalResult<ScriptResult>> ExecuteScriptAsync(
        string script, 
        Guid databaseId, 
        CancellationToken cancellationToken = default)
    {
        var request = new ExecuteScriptRequest
        {
            DatabaseId = databaseId,
            Script = script
        };
        
        using var httpClient = httpClientFactory.CreateClient();
        httpClient.BaseAddress = configuration.BaseUri;
        
        var response = await httpClient.PostAsJsonAsync("v1/script", request, cancellationToken);
        
        if (!response.IsSuccessStatusCode)
            return Result.Failed<ScriptResult>(await response.Content.ReadAsStringAsync(cancellationToken));
        
        var scriptResult = await response.Content.ReadFromJsonAsync<ScriptResult>(cancellationToken: cancellationToken);
        
        return scriptResult is not null 
            ? Result.Success(scriptResult) 
            : Result.Failed<ScriptResult>("An error occurred while executing the script.");
    }

    public async Task<IFinalResult<IReadOnlyCollection<ScriptResult>>> ExecuteScriptsAsync(IEnumerable<string> scripts, Guid databaseId, CancellationToken cancellationToken = default)
    {
        var request = new ExecuteScriptsRequest
        {
            DatabaseId = databaseId,
            Scripts = scripts
        };
        
        using var httpClient = httpClientFactory.CreateClient();
        httpClient.BaseAddress = configuration.BaseUri;
        
        var response = await httpClient.PostAsJsonAsync("v1/scripts", request, cancellationToken);
        
        if (!response.IsSuccessStatusCode)
            return Result.Failed<IReadOnlyCollection<ScriptResult>>(await response.Content.ReadAsStringAsync(cancellationToken));
        
        var scriptResult = await response.Content.ReadFromJsonAsync<IReadOnlyCollection<ScriptResult>>(cancellationToken: cancellationToken);
        
        return scriptResult is not null 
            ? Result.Success(scriptResult) 
            : Result.Failed<IReadOnlyCollection<ScriptResult>>("An error occurred while executing the script.");
    }
}