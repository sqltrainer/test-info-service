﻿using DatabaseHelper.ScriptResults;
using SqlTrainer.TestInfoService.Application.Services;

namespace SqlTrainer.TestInfoService.DatabasesInfrastructure.Services;

public sealed class ScriptResultCompareService : IScriptResultsCompareService
{
    public double Compare(ScriptResult actual, ScriptResult expected, double maxMark)
    {
        var actualObjectsCount = actual.Count();
        var expectedObjectsCount = expected.Count();
        
        // If there are no results, then the mark is 0
        if (actualObjectsCount == 0)
            return 0.0;
        
        // If the number of results is not equal, then the mark should be maximum a half of max
        var mark = maxMark;
        if (actualObjectsCount != expectedObjectsCount)
            mark -= mark / 2.0;

        // If the number of attributes is not equal, then the mark should be divided by 10
        var actualFirstObject = actual.First();
        var expectedFirstObject = expected.First();
        var actualAttributesCount = actualFirstObject.Count();
        var expectedAttributesCount = expectedFirstObject.Count();
        var attributesCountDifference = Math.Abs(actualAttributesCount - expectedAttributesCount);
        if (attributesCountDifference != 0)
            mark -= mark / 10.0;

        // If the attributes are different, then the mark should be divide by 10
        var actualAttributeNames = actualFirstObject.Select(o => o.Name);
        var expectedAttributeNames = expectedFirstObject.Select(o => o.Name);
        if (actualAttributeNames.Except(expectedAttributeNames).Count() > attributesCountDifference)
            mark -= mark / 10.0;

        // Calculate different data in script results
        var equaledCount = CountEqualed(actual, expected);
        var rowsCount = (double)Math.Max(actualObjectsCount, expectedObjectsCount);
        var attributesCount = (double)Math.Max(actualAttributesCount, expectedAttributesCount);
        var maxEqualedCount = rowsCount * attributesCount;

        var coefficient = equaledCount / maxEqualedCount;
        return Math.Ceiling(mark * coefficient * 100) / 100.0;
    }

    private static int CountEqualed(ScriptResult actual, ScriptResult expected)
    {
        var actualDictionary = actual
            .SelectMany(GetKeyValues)
            .GroupBy(kv => kv)
            .ToDictionary(g => g.Key, g => g.Count());

        var expectedDictionary = expected
            .SelectMany(GetKeyValues)
            .GroupBy(kv => kv)
            .ToDictionary(g => g.Key, g => g.Count());

        return CountEqualed(actualDictionary, expectedDictionary);
    }

    private static int CountEqualed(
        IDictionary<string, int> actualDictionary,
        IDictionary<string, int> expectedDictionary)
    {
        var count = 0;

        foreach (var thisKv in actualDictionary)
        {
            for (var i = 0; i < thisKv.Value; i++)
            {
                foreach (var otherKv in expectedDictionary)
                {
                    if (!thisKv.Key.Equals(otherKv.Key) || thisKv.Value < 0 || otherKv.Value < 0)
                        continue;
                    
                    actualDictionary[thisKv.Key] = thisKv.Value - 1;
                    expectedDictionary[otherKv.Key] = otherKv.Value - 1;
                    count++;
                }
            }
        }

        return count;

    }
    
    private static IEnumerable<string> GetKeyValues(ScriptResultObject objectValue) =>
        objectValue.Select(GetKeyValue);
    
    private static string GetKeyValue(ScriptResultObjectValue objectValue) =>
        $"Key: {objectValue.Name}, Value: {objectValue.Value}";
}