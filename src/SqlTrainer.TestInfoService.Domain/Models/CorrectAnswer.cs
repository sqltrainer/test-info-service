﻿using Domain.Models;

namespace SqlTrainer.TestInfoService.Domain.Models;

public sealed class CorrectAnswer(Guid id) : IdModel(id), IEquatable<CorrectAnswer>
{
    public CorrectAnswer() : this(default)
    {
    }
    
    public required string Body { get; set; }
    public required Guid QuestionId { get; init; }
    public Question? Question { get; private set; }

    public CorrectAnswer WithQuestion(Question question)
    {
        Question = question;
        return this;
    }
    
    public override bool Equals(object? obj) => obj is CorrectAnswer answer && Equals(answer);
    public bool Equals(CorrectAnswer? other) => other is not null && other.Id == Id;
    public override int GetHashCode() => base.GetHashCode();

    public static bool operator ==(CorrectAnswer? left, CorrectAnswer? right) => left is not null && left.Equals(right);
    public static bool operator !=(CorrectAnswer? left, CorrectAnswer? right) => !(left == right);

    public override string ToString() => $"{Body} ({Question?.Body})";
}