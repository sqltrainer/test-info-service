﻿using Domain.Models;

namespace SqlTrainer.TestInfoService.Domain.Models;

public sealed class Question(Guid id) : IdModel(id), IEquatable<Question>
{
    public Question() : this(default)
    {
    }
    
    public required string Body { get; set; }
    public required int Complexity { get; set; }
    public required Guid TopicId { get; set; }
    public required Guid DatabaseId { get; set; }
    public Topic? Topic { get; private set; }
    public CorrectAnswer? CorrectAnswer { get; private set; }

    public Question WithCorrectAnswer(CorrectAnswer correctAnswer)
    {
        CorrectAnswer = correctAnswer;
        return this;
    }
    
    public Question WithTopic(Topic topic)
    {
        Topic = topic;
        return this;
    }
    
    public override bool Equals(object? obj) => obj is Question question && Equals(question);
    public bool Equals(Question? other) => other is not null && other.Id == Id;
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Question? left, Question? right) => left is not null && left.Equals(right);
    public static bool operator !=(Question? left, Question? right) => !(left == right);
    
    public override string ToString() => Body;
}