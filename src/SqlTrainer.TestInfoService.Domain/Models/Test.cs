﻿using Domain.Models;

namespace SqlTrainer.TestInfoService.Domain.Models;

public sealed class Test(Guid id) : IdModel(id), IEquatable<Test>
{
    private readonly List<TestQuestion> testQuestions = [];
    
    public Test() : this(default)
    {
    }
    
    public required string Name { get; set; }
    public DateTimeOffset CreatedAt { get; init; } = DateTimeOffset.UtcNow;
    public IReadOnlyCollection<TestQuestion> TestQuestions => testQuestions.AsReadOnly();

    public void AddTestQuestions(IEnumerable<TestQuestion> tqs)
    {
        foreach (var tq in tqs)
            AddTestQuestion(tq);
    }

    public void AddTestQuestion(TestQuestion tq)
    {
        if (tq.QuestionId == default || tq.TestId == default)
        {
            testQuestions.Add(tq);
            return;
        }

        if (testQuestions.All(t => t.QuestionId != tq.QuestionId || t.TestId != tq.TestId))
            testQuestions.Add(tq);
    }

    public void ClearTestQuestions() => testQuestions.Clear();

    public override bool Equals(object? obj) => obj is Test test && Equals(test);
    public bool Equals(Test? other) => other is not null && (other.Id == Id || other.Name.Equals(Name));
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Test? left, Test? right) => left is not null && left.Equals(right);
    public static bool operator !=(Test? left, Test? right) => !(left == right);
    
    public override string ToString() => Name;
}