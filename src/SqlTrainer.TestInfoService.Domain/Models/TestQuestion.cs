﻿using Domain.Models;

namespace SqlTrainer.TestInfoService.Domain.Models;

public sealed class TestQuestion(Guid testId, Guid questionId) : Model, IEquatable<TestQuestion>
{
    public TestQuestion() : this(default, default)
    {
    }
    
    public Guid TestId { get; } = testId;
    public Guid QuestionId { get; } = questionId;
    public required double MaxMark { get; init; }
    public Question? Question { get; private set; }
    public Test? Test { get; private set; }

    public TestQuestion WithQuestion(Question question)
    {
        Question = question;
        return this;
    }
    
    public TestQuestion WithTest(Test test)
    {
        Test = test;
        return this;
    }
    
    public override bool Equals(object? obj) => obj is TestQuestion testQuestion && Equals(testQuestion);
    public bool Equals(TestQuestion? other) => other is not null && other.TestId == TestId && other.QuestionId == QuestionId;
    public override int GetHashCode() => HashCode.Combine(QuestionId, TestId);
    
    public static bool operator ==(TestQuestion? left, TestQuestion? right) => left is not null && left.Equals(right);
    public static bool operator !=(TestQuestion? left, TestQuestion? right) => !(left == right);
    
    public override string ToString() => $"{Question?.Body ?? QuestionId.ToString()} ({Test?.Name ?? TestId.ToString()}) - {MaxMark}";
}