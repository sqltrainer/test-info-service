﻿using Domain.Models;

namespace SqlTrainer.TestInfoService.Domain.Models;

public sealed class Topic(Guid id) : IdModel(id), IEquatable<Topic>
{
    public Topic() : this(default)
    {
    }
    
    public required string Name { get; init; }
    public IReadOnlyCollection<Question>? Questions { get; private set; }

    public Topic WithQuestions(IReadOnlyCollection<Question> questions)
    {
        Questions = questions;
        return this;
    }
    
    public override bool Equals(object? obj) => obj is Topic topic && Equals(topic);
    public bool Equals(Topic? other) => other is not null && (other.Id == Id || other.Name.Equals(Name));
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Topic? left, Topic? right) => left is not null && left.Equals(right);
    public static bool operator !=(Topic? left, Topic? right) => !(left == right);
    
    public override string ToString() => Name;
}