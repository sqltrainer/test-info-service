﻿using Domain.Models;

namespace SqlTrainer.TestInfoService.Domain.Models;

public sealed class UserAnswer(Guid scheduleTestId, Guid questionId, Guid userId) : Model, IEquatable<UserAnswer>
{
    public UserAnswer() : this(default, default, default)
    {
    }
    
    public Guid ScheduleTestId { get; } = scheduleTestId;
    public Guid QuestionId { get; } = questionId;
    public Guid UserId { get; } = userId;
    public required string Body { get; init; }
    public required double ProgramMark { get; init; }
    public double? TeacherMark { get; set; }
    public double? QuestionMaxMark { get; init; }
    public required DateTimeOffset AnsweredAt { get; init; }
    public bool LowConfidence { get; init; }
    public string? Comment { get; init; }
    public UserScheduleTest? UserScheduleTest { get; private set; }
    public ScheduleTest? ScheduleTest => UserScheduleTest?.ScheduleTest;
    public Question? Question { get; private set; }

    public UserAnswer WithUserScheduleTest(UserScheduleTest userScheduleTest)
    {
        UserScheduleTest = userScheduleTest;
        return this;
    }
    
    public UserAnswer WithQuestion(Question question)
    {
        Question = question;
        return this;
    }
    
    public override bool Equals(object? obj) => obj is UserAnswer userAnswer && Equals(userAnswer);
    public bool Equals(UserAnswer? other) => other is not null && other.ScheduleTestId == ScheduleTestId && other.QuestionId == QuestionId && other.UserId == UserId;
    public override int GetHashCode() => HashCode.Combine(ScheduleTestId, QuestionId, UserId);
    
    public static bool operator ==(UserAnswer? left, UserAnswer? right) => left is not null && left.Equals(right);
    public static bool operator !=(UserAnswer? left, UserAnswer? right) => !(left == right);
    
    public override string ToString() => $"User: {UserId}, Question: {Question?.ToString() ?? QuestionId.ToString()}, Schedule Test: {ScheduleTest?.ToString() ?? ScheduleTestId.ToString()} - {ProgramMark}/{TeacherMark}/{QuestionMaxMark}";
}