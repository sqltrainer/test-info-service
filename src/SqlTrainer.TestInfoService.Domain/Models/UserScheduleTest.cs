﻿using Domain.Models;

namespace SqlTrainer.TestInfoService.Domain.Models;

public sealed class UserScheduleTest(Guid scheduleTestId, Guid userId) : Model, IEquatable<UserScheduleTest>
{
    public UserScheduleTest() : this(default, default)
    {
    }
    
    public Guid ScheduleTestId { get; } = scheduleTestId;
    public Guid UserId { get; } = userId;
    public DateTimeOffset? FinishedAt { get; private set; }
    public bool? CheckedByTeacher { get; private set; }
    public double? ProgramMark { get; init; }
    public double? TeacherMark { get; init; }
    public double? QuestionMaxMark { get; init; }
    public ScheduleTest? ScheduleTest { get; private set; }
    
    public void UserFinishedTest(DateTimeOffset finishedAt)
    {
        if (FinishedAt.HasValue)
            throw new InvalidOperationException("User has already finished the test earlier");
        
        FinishedAt = finishedAt;
    }
    
    public void SetTeacherChecked(bool checkedByTeacher)
    {
        CheckedByTeacher = checkedByTeacher;
    }

    public UserScheduleTest WithScheduleTest(ScheduleTest scheduleTest)
    {
        ScheduleTest = scheduleTest;
        return this;
    }
    
    public override bool Equals(object? obj) => obj is UserScheduleTest userScheduleTest && Equals(userScheduleTest);
    public bool Equals(UserScheduleTest? other) => other is not null && other.ScheduleTestId == ScheduleTestId && other.UserId == UserId;
    public override int GetHashCode() => HashCode.Combine(ScheduleTestId, UserId);
    
    public static bool operator ==(UserScheduleTest? left, UserScheduleTest? right) => left is not null && left.Equals(right);
    public static bool operator !=(UserScheduleTest? left, UserScheduleTest? right) => !(left == right);
    
    public override string ToString() => $"User: {UserId}, Schedule test: {ScheduleTestId}";
}