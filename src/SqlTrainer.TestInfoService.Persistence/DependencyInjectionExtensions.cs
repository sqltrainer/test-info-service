﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlTrainer.TestInfoService.Application.DAL;

namespace SqlTrainer.TestInfoService.Persistence;

public static class DependencyInjectionExtensions
{
    public static void UseTestInfoDb(this DbContextOptionsBuilder options, IConfiguration configuration)
    {
        options.UseNpgsql(configuration.GetConnectionString("TestInfoDb"));
    }

    public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<TestInfoDbContext>(o => o.UseTestInfoDb(configuration)); 
        
        return services.AddScoped<ITestInfoDbContext, TestInfoDbContext>();
    }
}