﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SqlTrainer.TestInfoService.Persistence.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tests",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    created_at = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("tests_id_pk", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "topics",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("topics_id_pk", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "schedule_tests",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    test_id = table.Column<Guid>(type: "uuid", nullable: false),
                    start_at = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    finish_at = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("scheduletests_id_pk", x => x.id);
                    table.ForeignKey(
                        name: "scheduletests_testid_test_fk",
                        column: x => x.test_id,
                        principalTable: "tests",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "questions",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    body = table.Column<string>(type: "character varying(5000)", maxLength: 5000, nullable: false),
                    complexity = table.Column<int>(type: "integer", nullable: false),
                    topic_id = table.Column<Guid>(type: "uuid", nullable: false),
                    database_id = table.Column<Guid>(type: "uuid", nullable: false),
                    TopicId1 = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("questions_id_pk", x => x.id);
                    table.ForeignKey(
                        name: "FK_questions_topics_TopicId1",
                        column: x => x.TopicId1,
                        principalTable: "topics",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "questions_topicid_topic_fk",
                        column: x => x.topic_id,
                        principalTable: "topics",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "user_schedule_tests",
                columns: table => new
                {
                    schedule_test_id = table.Column<Guid>(type: "uuid", nullable: false),
                    user_id = table.Column<Guid>(type: "uuid", nullable: false),
                    started_at = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    checked_by_teacher = table.Column<bool>(type: "boolean", nullable: true, defaultValue: false),
                    ProgramMark = table.Column<double>(type: "double precision", nullable: true),
                    TeacherMark = table.Column<double>(type: "double precision", nullable: true),
                    QuestionMaxMark = table.Column<double>(type: "double precision", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("user_schedule_tests_id_pk", x => new { x.schedule_test_id, x.user_id });
                    table.ForeignKey(
                        name: "user_schedule_tests_schedule_test_id_schedule_tests_fk",
                        column: x => x.schedule_test_id,
                        principalTable: "schedule_tests",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "correct_answers",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    body = table.Column<string>(type: "character varying(5000)", maxLength: 5000, nullable: false),
                    question_id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("correctanswers_id_pk", x => x.id);
                    table.ForeignKey(
                        name: "correctanswers_questionid_question_fk",
                        column: x => x.question_id,
                        principalTable: "questions",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "test_questions",
                columns: table => new
                {
                    test_id = table.Column<Guid>(type: "uuid", nullable: false),
                    question_id = table.Column<Guid>(type: "uuid", nullable: false),
                    max_mark = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("testquestions_testid_questionid_pk", x => new { x.test_id, x.question_id });
                    table.ForeignKey(
                        name: "testquestions_questionid_questions_fk",
                        column: x => x.question_id,
                        principalTable: "questions",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "testquestions_testid_tests_fk",
                        column: x => x.test_id,
                        principalTable: "tests",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "user_answers",
                columns: table => new
                {
                    schedule_test_id = table.Column<Guid>(type: "uuid", nullable: false),
                    question_id = table.Column<Guid>(type: "uuid", nullable: false),
                    user_id = table.Column<Guid>(type: "uuid", nullable: false),
                    Body = table.Column<string>(type: "character varying(5000)", maxLength: 5000, nullable: false),
                    ProgramMark = table.Column<double>(type: "double precision", nullable: false),
                    TeacherMark = table.Column<double>(type: "double precision", nullable: true),
                    QuestionMaxMark = table.Column<double>(type: "double precision", nullable: true),
                    answered_at = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    low_confidence = table.Column<bool>(type: "boolean", nullable: false),
                    comment = table.Column<string>(type: "character varying(5000)", maxLength: 5000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("useranswers_userid_questionid_scheduletestid_pk", x => new { x.user_id, x.question_id, x.schedule_test_id });
                    table.ForeignKey(
                        name: "FK_user_answers_user_schedule_tests_schedule_test_id_user_id",
                        columns: x => new { x.schedule_test_id, x.user_id },
                        principalTable: "user_schedule_tests",
                        principalColumns: new[] { "schedule_test_id", "user_id" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "useranswers_questionid_questions_fk",
                        column: x => x.question_id,
                        principalTable: "questions",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_correct_answers_question_id",
                table: "correct_answers",
                column: "question_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_questions_topic_id",
                table: "questions",
                column: "topic_id");

            migrationBuilder.CreateIndex(
                name: "IX_questions_TopicId1",
                table: "questions",
                column: "TopicId1");

            migrationBuilder.CreateIndex(
                name: "IX_schedule_tests_test_id",
                table: "schedule_tests",
                column: "test_id");

            migrationBuilder.CreateIndex(
                name: "IX_test_questions_question_id",
                table: "test_questions",
                column: "question_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_answers_question_id",
                table: "user_answers",
                column: "question_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_answers_schedule_test_id_user_id",
                table: "user_answers",
                columns: new[] { "schedule_test_id", "user_id" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "correct_answers");

            migrationBuilder.DropTable(
                name: "test_questions");

            migrationBuilder.DropTable(
                name: "user_answers");

            migrationBuilder.DropTable(
                name: "user_schedule_tests");

            migrationBuilder.DropTable(
                name: "questions");

            migrationBuilder.DropTable(
                name: "schedule_tests");

            migrationBuilder.DropTable(
                name: "topics");

            migrationBuilder.DropTable(
                name: "tests");
        }
    }
}
