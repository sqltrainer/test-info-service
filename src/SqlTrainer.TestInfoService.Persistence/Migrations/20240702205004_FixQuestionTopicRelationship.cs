﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SqlTrainer.TestInfoService.Persistence.Migrations
{
    /// <inheritdoc />
    public partial class FixQuestionTopicRelationship : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_questions_topics_TopicId1",
                table: "questions");

            migrationBuilder.DropIndex(
                name: "IX_questions_TopicId1",
                table: "questions");

            migrationBuilder.DropColumn(
                name: "TopicId1",
                table: "questions");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "TopicId1",
                table: "questions",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_questions_TopicId1",
                table: "questions",
                column: "TopicId1");

            migrationBuilder.AddForeignKey(
                name: "FK_questions_topics_TopicId1",
                table: "questions",
                column: "TopicId1",
                principalTable: "topics",
                principalColumn: "id");
        }
    }
}
