﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Persistence;

public class TestInfoDbContext(DbContextOptions<TestInfoDbContext> options) : DbContext(options), ITestInfoDbContext
{
    public DbSet<Topic> Topics { get; set; }
    public DbSet<Test> Tests { get; set; }
    public DbSet<Question> Questions { get; set; }
    public DbSet<TestQuestion> TestQuestions { get; set; }
    public DbSet<CorrectAnswer> CorrectAnswers { get; set; }
    public DbSet<ScheduleTest> ScheduleTests { get; set; }
    public DbSet<UserScheduleTest> UserScheduleTests { get; set; }
    public DbSet<UserAnswer> UserAnswers { get; set; }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        ConfigureTopics(modelBuilder.Entity<Topic>());
        ConfigureTests(modelBuilder.Entity<Test>());
        ConfigureQuestions(modelBuilder.Entity<Question>());
        ConfigureTestQuestions(modelBuilder.Entity<TestQuestion>());
        ConfigureCorrectAnswers(modelBuilder.Entity<CorrectAnswer>());
        ConfigureScheduleTests(modelBuilder.Entity<ScheduleTest>());
        ConfigureUserScheduleTests(modelBuilder.Entity<UserScheduleTest>());
        ConfigureUserAnswers(modelBuilder.Entity<UserAnswer>());
    }
    
    private static void ConfigureTopics(EntityTypeBuilder<Topic> entityTypeBuilder)
    {
        entityTypeBuilder.ToTable("topics");
        
        entityTypeBuilder.HasKey(e => e.Id).HasName("topics_id_pk");
        entityTypeBuilder.Property(e => e.Id).ValueGeneratedOnAdd().HasColumnName("id");
        
        entityTypeBuilder.Property(e => e.Name).HasColumnName("name").HasMaxLength(500);
    }
    
    private static void ConfigureTests(EntityTypeBuilder<Test> entityTypeBuilder)
    {
        entityTypeBuilder.ToTable("tests");
        
        entityTypeBuilder.HasKey(e => e.Id).HasName("tests_id_pk");
        entityTypeBuilder.Property(e => e.Id).ValueGeneratedOnAdd().HasColumnName("id");
        
        entityTypeBuilder.Property(e => e.Name).HasColumnName("name").HasMaxLength(500).IsRequired();
        
        entityTypeBuilder.Property(e => e.CreatedAt)
            .HasColumnName("created_at")
            .HasColumnType("timestamp with time zone")
            .HasConversion(e => e.ToUniversalTime(), e => e)
            .IsRequired();
    }
    
    private static void ConfigureQuestions(EntityTypeBuilder<Question> entityTypeBuilder)
    {
        entityTypeBuilder.ToTable("questions");
        
        entityTypeBuilder.HasKey(e => e.Id).HasName("questions_id_pk");
        entityTypeBuilder.Property(e => e.Id).ValueGeneratedOnAdd().HasColumnName("id");
        
        entityTypeBuilder.Property(e => e.Body).HasColumnName("body").HasMaxLength(5000).IsRequired();
        
        entityTypeBuilder.Property(e => e.Complexity).HasColumnName("complexity").IsRequired();
        
        entityTypeBuilder.Property(e => e.TopicId).HasColumnName("topic_id").IsRequired();
        
        entityTypeBuilder.Property(e => e.DatabaseId).HasColumnName("database_id").IsRequired();
        
        entityTypeBuilder.HasOne(e => e.Topic)
            .WithMany(e => e.Questions)
            .HasForeignKey(e => e.TopicId)
            .HasConstraintName("questions_topicid_topic_fk");
    }
    
    private static void ConfigureTestQuestions(EntityTypeBuilder<TestQuestion> entityTypeBuilder)
    {
        entityTypeBuilder.ToTable("test_questions");
        
        entityTypeBuilder.Property(e => e.TestId).HasColumnName("test_id").IsRequired();
        entityTypeBuilder.Property(e => e.QuestionId).HasColumnName("question_id").IsRequired();
        
        entityTypeBuilder.HasKey(nameof(TestQuestion.TestId), nameof(TestQuestion.QuestionId)).HasName("testquestions_testid_questionid_pk");
        
        entityTypeBuilder.Property(e => e.MaxMark).HasColumnName("max_mark").IsRequired();
        
        entityTypeBuilder.HasOne(e => e.Test)
            .WithMany(e => e.TestQuestions)
            .HasForeignKey(e => e.TestId)
            .HasConstraintName("testquestions_testid_tests_fk");
        
        entityTypeBuilder.HasOne(e => e.Question)
            .WithMany()
            .HasForeignKey(e => e.QuestionId)
            .HasConstraintName("testquestions_questionid_questions_fk");
    }
    
    private static void ConfigureCorrectAnswers(EntityTypeBuilder<CorrectAnswer> entityTypeBuilder)
    {
        entityTypeBuilder.ToTable("correct_answers");
        
        entityTypeBuilder.HasKey(e => e.Id).HasName("correctanswers_id_pk");
        entityTypeBuilder.Property(e => e.Id).ValueGeneratedOnAdd().HasColumnName("id");
        
        entityTypeBuilder.Property(e => e.Body).HasColumnName("body").HasMaxLength(5000).IsRequired();
        
        entityTypeBuilder.Property(e => e.QuestionId).HasColumnName("question_id").IsRequired();
        
        entityTypeBuilder.HasOne(e => e.Question)
            .WithOne(e => e.CorrectAnswer)
            .HasForeignKey<CorrectAnswer>(e => e.QuestionId)
            .HasConstraintName("correctanswers_questionid_question_fk")
            .IsRequired();
    }
    
    private static void ConfigureScheduleTests(EntityTypeBuilder<ScheduleTest> entityTypeBuilder)
    {
        entityTypeBuilder.ToTable("schedule_tests");
        
        entityTypeBuilder.HasKey(e => e.Id).HasName("scheduletests_id_pk");
        entityTypeBuilder.Property(e => e.Id).ValueGeneratedOnAdd().HasColumnName("id");
        
        entityTypeBuilder.Property(e => e.StartAt)
            .HasColumnName("start_at")
            .HasColumnType("timestamp with time zone")
            .HasConversion(e => e.ToUniversalTime(), e => e)
            .IsRequired();
        
        entityTypeBuilder.Property(e => e.FinishAt)
            .HasColumnName("finish_at")
            .HasColumnType("timestamp with time zone")
            .HasConversion(e => e.ToUniversalTime(), e => e)
            .IsRequired();
        
        entityTypeBuilder.Property(e => e.TestId).HasColumnName("test_id").IsRequired();
        
        entityTypeBuilder.HasOne(st => st.Test)
            .WithMany()
            .HasForeignKey(e => e.TestId)
            .HasConstraintName("scheduletests_testid_test_fk");
    }
    
    private static void ConfigureUserScheduleTests(EntityTypeBuilder<UserScheduleTest> entityTypeBuilder)
    {
        entityTypeBuilder.ToTable("user_schedule_tests");
        
        entityTypeBuilder.Property(e => e.ScheduleTestId).HasColumnName("schedule_test_id").IsRequired();
        entityTypeBuilder.Property(e => e.UserId).HasColumnName("user_id").IsRequired();
        
        entityTypeBuilder.HasKey(nameof(UserScheduleTest.ScheduleTestId), nameof(UserScheduleTest.UserId)).HasName("user_schedule_tests_id_pk");
        
        entityTypeBuilder.Property(e => e.FinishedAt)
            .HasColumnName("started_at")
            .HasColumnType("timestamp with time zone")
            .HasConversion(e => e.HasValue ? e.Value.ToUniversalTime() : (DateTimeOffset?)null, e => e);

        entityTypeBuilder.Property(e => e.CheckedByTeacher).HasColumnName("checked_by_teacher").HasDefaultValue(false);
        
        entityTypeBuilder.HasOne(e => e.ScheduleTest)
            .WithMany(e => e.UserScheduleTests)
            .HasForeignKey(e => e.ScheduleTestId)
            .HasConstraintName("user_schedule_tests_schedule_test_id_schedule_tests_fk");
    }
    
    private static void ConfigureUserAnswers(EntityTypeBuilder<UserAnswer> entityTypeBuilder)
    {
        entityTypeBuilder.ToTable("user_answers");
        
        entityTypeBuilder.Property(e => e.UserId).HasColumnName("user_id").IsRequired();
        entityTypeBuilder.Property(e => e.QuestionId).HasColumnName("question_id").IsRequired();
        entityTypeBuilder.Property(e => e.ScheduleTestId).HasColumnName("schedule_test_id").IsRequired();
        
        entityTypeBuilder.HasKey(nameof(UserAnswer.UserId), nameof(UserAnswer.QuestionId), nameof(UserAnswer.ScheduleTestId)).HasName("useranswers_userid_questionid_scheduletestid_pk");

        entityTypeBuilder.Property(e => e.Body).HasMaxLength(5000).IsRequired();

        entityTypeBuilder.Property(e => e.ProgramMark).IsRequired();
        entityTypeBuilder.Property(e => e.TeacherMark).IsRequired(false);

        entityTypeBuilder.Property(e => e.AnsweredAt)
            .HasColumnName("answered_at")
            .HasColumnType("timestamp with time zone")
            .HasConversion(e => e.ToUniversalTime(), e => e)
            .IsRequired();
        
        entityTypeBuilder.Property(e => e.LowConfidence).HasColumnName("low_confidence").IsRequired();
        entityTypeBuilder.Property(e => e.Comment).HasColumnName("comment").HasMaxLength(5000);
        
        entityTypeBuilder.HasOne(e => e.UserScheduleTest)
            .WithMany()
            .HasForeignKey(nameof(UserScheduleTest.ScheduleTestId), nameof(UserScheduleTest.UserId));
        
        entityTypeBuilder.HasOne(e => e.Question)
            .WithMany()
            .HasForeignKey(e => e.QuestionId)
            .HasConstraintName("useranswers_questionid_questions_fk");
    }
}