﻿using Microsoft.AspNetCore.Mvc;
using Results.WebExtensions;
using SqlTrainer.TestInfoService.Application.UserAnswers;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Endpoints;

internal static class AnswersEndpoints
{
    internal static void MapAnswersEndpoints(this WebApplication app)
    {
        var version1 = app.MapGroup("v1/users/{userId:guid}/schedule-tests/{scheduleTestId:guid}/answers");
        
        version1.MapPost("/", CreateAsync)
            .WithName("CreateAnswers")
            .WithDisplayName("Create Answers")
            .WithDescription("Create new answers.")
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapPut("/", UpdateAsync)
            .WithName("UpdateAnswer")
            .WithDisplayName("Update Answer")
            .WithDescription("Update the answer.")
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapGet("", GetUserTestAnswersAsync)
            .WithName("GetUserTestAnswers")
            .WithDisplayName("Get User Test Answers")
            .WithDescription("Get user test answers.")
            .Produces<IReadOnlyCollection<UserAnswer>>()
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
    }
    
    private static async Task<IResult> CreateAsync(
        [FromServices] IAddUserAnswersRequestHandler handler,
        [FromRoute] Guid userId,
        [FromRoute] Guid scheduleTestId,
        [FromBody] AddUserAnswersRequest request,
        CancellationToken cancellationToken = default)
    {
        request = request with { UserId = userId, ScheduleTestId = scheduleTestId };
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> UpdateAsync(
        [FromServices] IUpdateUserAnswerRequestHandler handler,
        [FromBody] UpdateUserAnswerRequest request,
        CancellationToken cancellationToken = default)
    {
        request = request with { UserId = request.UserId, ScheduleTestId = request.ScheduleTestId };
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetUserTestAnswersAsync(
        [FromServices] IGetByScheduleTestAndUserAnswersRequestHandler handler,
        [FromRoute] Guid userId,
        [FromRoute] Guid scheduleTestId,
        CancellationToken cancellationToken = default)
    {
        var request = new GetByScheduleTestAndUserAnswersRequest(userId, scheduleTestId);
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
}