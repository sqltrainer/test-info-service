﻿using HttpServer.Requests;
using Microsoft.AspNetCore.Mvc;
using Results.WebExtensions;
using SqlTrainer.TestInfoService.Application.Questions;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Endpoints;

internal static class QuestionsEndpoints
{
    private const string Version1 = "v1/questions";
    
    internal static void MapQuestionsEndpoints(this WebApplication app)
    {
        var version1 = app.MapGroup(Version1);
        
        version1.MapPost("/", CreateAsync)
            .WithName("CreateQuestion")
            .WithDisplayName("Create Question")
            .WithDescription("Create a new question.")
            .Produces<Guid>(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapDelete("/", DeleteAsync)
            .WithName("DeleteQuestion")
            .WithDisplayName("Delete Question")
            .WithDescription("Delete the question.")
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapPut("/", UpdateAsync)
            .WithName("UpdateQuestion")
            .WithDisplayName("Update Question")
            .WithDescription("Update the question.")
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapGet("/", GetAllAsync)
            .WithName("GetAllQuestions")
            .WithDisplayName("Get All Questions")
            .WithDescription("Get all questions.")
            .Produces<IReadOnlyCollection<Question>>()
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
    }

    private static async Task<IResult> CreateAsync(
        [FromServices] IAddQuestionRequestHandler handler,
        [FromBody] AddQuestionRequest request,
        CancellationToken cancellationToken = default)
    {
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.IsSucceeded ? Microsoft.AspNetCore.Http.Results.Created($"{Version1}/{result.Value}", result.Value) : result.ToAspNetResult();
    }

    private static async Task<IResult> DeleteAsync(
        [FromServices] IDeleteQuestionRequestHandler handler,
        [FromBody] DeleteQuestionRequest request,
        CancellationToken cancellationToken = default)
    {
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> UpdateAsync(
        [FromServices] IUpdateQuestionRequestHandler handler,
        [FromBody] UpdateQuestionRequest request,
        CancellationToken cancellationToken = default)
    {
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetAllAsync(
        [FromServices] IGetAllQuestionsRequestHandler handler,
        FilterPaginatedQuery query,
        CancellationToken cancellationToken = default)
    {
        var request = new GetAllQuestionsRequest(query.Search, query.SortBy, query.SortDirection, query.Page, query.PageSize);
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
}