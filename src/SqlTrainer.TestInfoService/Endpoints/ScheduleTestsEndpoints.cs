﻿using HttpServer.Requests;
using Microsoft.AspNetCore.Mvc;
using Results.WebExtensions;
using SqlTrainer.TestInfoService.Application.Questions;
using SqlTrainer.TestInfoService.Application.ScheduleTests;
using SqlTrainer.TestInfoService.Application.UserScheduleTests;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Endpoints;

internal static class ScheduleTestsEndpoints
{
    private const string Version1 = "v1/schedule-tests";

    internal static void MapScheduleTestsEndpoints(this WebApplication app)
    {
        var version1 = app.MapGroup(Version1);
        var userScheduleTestsVersion1 = app.MapGroup("v1/users/{userId:guid}/schedule-tests");
        
        version1.MapPost("/", CreateAsync)
            .WithName("CreateScheduleTest")
            .WithDisplayName("Create Schedule Test")
            .WithDescription("Create a new schedule test.")
            .Produces<Guid>(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);

        version1.MapDelete("/", DeleteAsync)
            .WithName("DeleteScheduleTest")
            .WithDisplayName("Delete Schedule Test")
            .WithDescription("Delete the schedule test.")
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapPut("/", UpdateAsync)
            .WithName("UpdateScheduleTest")
            .WithDisplayName("Update Schedule Test")
            .WithDescription("Update the schedule test.")
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapGet("/", GetAllAsync)
            .WithName("GetAllScheduleTests")
            .WithDisplayName("Get All Schedule Tests")
            .WithDescription("Get all schedule tests.")
            .Produces<IReadOnlyCollection<ScheduleTest>>()
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapGet("/{scheduleTestId:guid}", GetByIdAsync)
            .WithName("GetScheduleTestById")
            .WithDisplayName("Get Schedule Test by Id")
            .WithDescription("Get schedule test by Id.")
            .Produces<ScheduleTest>()
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status404NotFound)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapGet("/{scheduleTestId:guid}/questions", GetQuestionsByIdAsync)
            .WithName("GetQuestionsByScheduleTestId")
            .WithDisplayName("Get Questions by Schedule Test Id")
            .WithDescription("Get questions by schedule test Id.")
            .Produces<IReadOnlyCollection<Question>>()
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        userScheduleTestsVersion1.MapGet("/", GetByUserIdAsync)
            .WithName("GetScheduleTestsByUserId")
            .WithDisplayName("Get Schedule Tests by User Id")
            .WithDescription("Get schedule tests by user Id.")
            .Produces<IReadOnlyCollection<ScheduleTest>>()
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        userScheduleTestsVersion1.MapPut("/", UpdateUserScheduleTestAsync)
            .WithName("UpdateUserScheduleTest")
            .WithDisplayName("Update User Schedule Test")
            .WithDescription("Update user schedule test.")
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
    }
    
    private static async Task<IResult> CreateAsync(
        [FromServices] IAddScheduleTestRequestHandler handler,
        [FromBody] AddScheduleTestRequest request,
        CancellationToken cancellationToken = default)
    {
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.IsSucceeded ? Microsoft.AspNetCore.Http.Results.Created($"{Version1}/{result.Value}", result.Value) : result.ToAspNetResult();
    }
    
    private static async Task<IResult> DeleteAsync(
        [FromServices] IDeleteScheduleTestRequestHandler handler,
        [FromBody] DeleteScheduleTestRequest request,
        CancellationToken cancellationToken = default)
    {
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> UpdateAsync(
        [FromServices] IUpdateScheduleTestRequestHandler handler,
        [FromBody] UpdateScheduleTestRequest request,
        CancellationToken cancellationToken = default)
    {
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetAllAsync(
        [FromServices] IGetAllScheduleTestsRequestHandler handler,
        FilterPaginatedQuery query,
        CancellationToken cancellationToken = default)
    {
        var request = new GetAllScheduleTestsRequest(query.Search, query.SortBy, query.SortDirection, query.Page, query.PageSize);
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetByIdAsync(
        [FromServices] IGetByIdScheduleTestRequestHandler handler,
        [FromRoute] Guid scheduleTestId,
        CancellationToken cancellationToken = default)
    {
        var request = new GetByIdScheduleTestRequest(scheduleTestId);
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetQuestionsByIdAsync(
        [FromServices] IGetQuestionsByScheduleTestRequestHandler handler,
        [FromRoute] Guid scheduleTestId,
        CancellationToken cancellationToken = default)
    {
        var request = new GetQuestionsByScheduleTestRequest(scheduleTestId);
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetByUserIdAsync(
        [FromServices] IGetByUserScheduleTestsRequestHandler handler,
        [FromRoute] Guid userId,
        FilterPaginatedQuery query,
        CancellationToken cancellationToken = default)
    {
        var request = new GetByUserScheduleTestsRequest(userId, query.Search, query.SortBy, query.SortDirection, query.Page, query.PageSize);
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> UpdateUserScheduleTestAsync(
        [FromServices] IUpdateUserScheduleTestRequestHandler handler,
        [FromRoute] Guid userId,
        [FromBody] UpdateUserScheduleTestRequest request,
        CancellationToken cancellationToken = default)
    {
        request = request with { UserId = userId };
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
}