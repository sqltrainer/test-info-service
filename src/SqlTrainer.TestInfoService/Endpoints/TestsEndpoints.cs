﻿using HttpServer.Requests;
using Microsoft.AspNetCore.Mvc;
using Results.WebExtensions;
using SqlTrainer.TestInfoService.Application.Tests;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Endpoints;

internal static class TestsEndpoints
{
    private const string Version1 = "v1/tests";
    
    internal static void MapTestsEndpoints(this WebApplication app)
    {
        var version1 = app.MapGroup(Version1);
        
        version1.MapPost("/", CreateAsync)
            .WithName("CreateTest")
            .WithDisplayName("Create Test")
            .WithDescription("Create a new test.")
            .Produces<Guid>(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapDelete("/", DeleteAsync)
            .WithName("DeleteTest")
            .WithDisplayName("Delete Test")
            .WithDescription("Delete the test.")
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapPut("/", UpdateAsync)
            .WithName("UpdateTest")
            .WithDisplayName("Update Test")
            .WithDescription("Update the test.")
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapGet("/", GetAllAsync)
            .WithName("GetAllTests")
            .WithDisplayName("Get All Tests")
            .WithDescription("Get all tests.")
            .Produces<IReadOnlyCollection<Test>>()
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapGet("/{id:guid}", GetByIdAsync)
            .WithName("GetTest")
            .WithDisplayName("Get Test")
            .WithDescription("Get the test.")
            .Produces<Test>()
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status404NotFound)
            .Produces(StatusCodes.Status500InternalServerError);
    }
    
    private static async Task<IResult> CreateAsync(
        [FromServices] IAddTestRequestHandler handler,
        [FromBody] AddTestRequest request,
        CancellationToken cancellationToken = default)
    {
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.IsSucceeded ? Microsoft.AspNetCore.Http.Results.Created($"{Version1}/{result.Value}", result.Value) : result.ToAspNetResult();
    }
    
    private static async Task<IResult> DeleteAsync(
        [FromServices] IDeleteTestRequestHandler handler,
        [FromBody] DeleteTestRequest request,
        CancellationToken cancellationToken = default)
    {
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.IsSucceeded ? Microsoft.AspNetCore.Http.Results.NoContent() : result.ToAspNetResult();
    }
    
    private static async Task<IResult> UpdateAsync(
        [FromServices] IUpdateTestRequestHandler handler,
        [FromBody] UpdateTestRequest request,
        CancellationToken cancellationToken = default)
    {
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.IsSucceeded ? Microsoft.AspNetCore.Http.Results.NoContent() : result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetAllAsync(
        [FromServices] IGetAllTestsRequestHandler handler,
        FilterPaginatedQuery query,
        CancellationToken cancellationToken = default)
    {
        var request = new GetAllTestsRequest(query.Search, query.SortBy, query.SortDirection, query.Page, query.PageSize);
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetByIdAsync(
        [FromServices] IGetByIdTestRequestHandler handler,
        [FromRoute] Guid id,
        CancellationToken cancellationToken = default)
    {
        var request = new GetByIdTestRequest(id);
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
}