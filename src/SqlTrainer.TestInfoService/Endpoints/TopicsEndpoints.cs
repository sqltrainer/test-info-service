﻿using HttpServer.Requests;
using Microsoft.AspNetCore.Mvc;
using Results.WebExtensions;
using SqlTrainer.TestInfoService.Application.Topics;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Endpoints;

internal static class TopicsEndpoints
{
    internal static void MapTopicsEndpoints(this WebApplication app)
    {
        var version1 = app.MapGroup("v1/topics");
        
        version1.MapPost("/", CreateAsync)
            .WithName("CreateTopic")
            .WithDisplayName("Create Topic")
            .WithDescription("Create a new topic.")
            .Produces<Guid>(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapDelete("/{id:guid}", DeleteAsync)
            .WithName("DeleteTopic")
            .WithDisplayName("Delete Topic")
            .WithDescription("Delete the topic.")
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapPut("/", UpdateAsync)
            .WithName("UpdateTopic")
            .WithDisplayName("Update Topic")
            .WithDescription("Update the topic.")
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapGet("/", GetAllAsync)
            .WithName("GetAllTopics")
            .WithDisplayName("Get All Topics")
            .WithDescription("Get all topics.")
            .Produces<IReadOnlyCollection<Topic>>()
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
        
        version1.MapGet("/{id:guid}", GetByIdAsync)
            .WithName("GetTopic")
            .WithDisplayName("Get Topic")
            .WithDescription("Get the topic.")
            .Produces<Topic>()
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status404NotFound)
            .Produces(StatusCodes.Status500InternalServerError);
    }
    
    private static async Task<IResult> CreateAsync(
        [FromServices] IAddTopicRequestHandler handler,
        [FromBody] AddTopicRequest request,
        CancellationToken cancellationToken = default)
    {
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.IsSucceeded ? Microsoft.AspNetCore.Http.Results.Created($"v1/topics/{result.Value}", result.Value) : result.ToAspNetResult();
    }
    
    private static async Task<IResult> DeleteAsync(
        [FromServices] IDeleteTopicRequestHandler handler,
        [FromRoute] Guid id,
        CancellationToken cancellationToken = default)
    {
        DeleteTopicRequest request = new(id);
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> UpdateAsync(
        [FromServices] IUpdateTopicRequestHandler handler,
        [FromBody] UpdateTopicRequest request,
        CancellationToken cancellationToken = default)
    {
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetAllAsync(
        [FromServices] IGetAllTopicsRequestHandler handler,
        FilterPaginatedQuery query,
        CancellationToken cancellationToken = default)
    {
        var request = new GetAllTopicsRequest(query.Search, query.SortBy, query.SortDirection, query.Page, query.PageSize);
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetByIdAsync(
        [FromServices] IGetByIdTopicRequestHandler handler,
        [FromRoute] Guid id,
        CancellationToken cancellationToken = default)
    {
        var request = new GetByIdTopicRequest(id);
        var result = await handler.HandleAsync(request, cancellationToken);
        return result.ToAspNetResult();
    }
}