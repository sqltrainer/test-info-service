﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace SqlTrainer.TestInfoService.Persistence;

// ReSharper disable once UnusedType.Global
/// <summary>
/// The design time database context factory for the test info database context.
/// </summary>
/// <remarks>
/// It's used to create a database context for design time tools like EF Core migrations.
/// </remarks>
public sealed class DesignTimeTestInfoDbContextFactory : IDesignTimeDbContextFactory<TestInfoDbContext>
{
    public TestInfoDbContext CreateDbContext(string[] args)
    {
        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddUserSecrets(Assembly.GetExecutingAssembly())
            .AddEnvironmentVariables()
            .Build();
        
        var builder = new DbContextOptionsBuilder<TestInfoDbContext>();

        builder.UseTestInfoDb(configuration);

        return new TestInfoDbContext(builder.Options);
    }
}
