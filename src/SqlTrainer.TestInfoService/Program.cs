using SqlTrainer.TestInfoService.Application.Extensions;
using SqlTrainer.TestInfoService.Application.Services;
using SqlTrainer.TestInfoService.DatabaseServiceClient.Extensions;
using SqlTrainer.TestInfoService.DatabasesInfrastructure.Services;
using SqlTrainer.TestInfoService.Endpoints;
using SqlTrainer.TestInfoService.Persistence;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddEnvironmentVariables();

if (builder.Environment.IsDevelopment())
    builder.Configuration.AddUserSecrets<Program>();

builder.Services.AddHttpClient();

builder.Services.AddDatabaseServices(builder.Configuration);

builder.Services
    .AddPersistence(builder.Configuration)
    .AddTransient<IScriptFormattingService, ScriptFormattingService>()
    .AddTransient<IScriptResultsCompareService, ScriptResultCompareService>()
    .AddApplication();

builder.Services.AddEndpointsApiExplorer();

var app = builder.Build();

app.MapTopicsEndpoints();
app.MapQuestionsEndpoints();
app.MapTestsEndpoints();
app.MapScheduleTestsEndpoints();
app.MapAnswersEndpoints();

app.Run();