﻿using SqlTrainer.TestInfoService.DatabasesInfrastructure.Services;

namespace SqlTrainer.TestInfoService.DatabasesInfrastructure.Tests.Services;

public sealed class ScriptFormattingServiceTests
{
    private readonly ScriptFormattingService scriptFormattingService = new();

    [Fact]
    public void Clear_MultipleLinesComment_InTheEnd_Test()
    {
        const string script =
            """
            select *
            from Table;
            /* have some comments 
                that need to be removed*/
            """;

        var updatedScript = scriptFormattingService.Clear(script);

        const string expectedScript = "select * from Table;";

        updatedScript.Should().Be(expectedScript);
    }

    [Fact]
    public void Clear_MultipleLinesComment_InTheBegin_Test()
    {
        const string script =
            """
            /* have some comments 
                that need to be removed*/
            select *
            from Table;
            """;

        var updatedScript = scriptFormattingService.Clear(script);

        const string expectedScript = "select * from Table;";

        updatedScript.Should().Be(expectedScript);
    }
    
    [Fact]
    public void Clear_MultipleLinesComment_InTheMiddle_Test()
    {
        const string script =
            """
            select *
            /* have some comments 
                that need to be removed*/
            from Table;
            """;

        var updatedScript = scriptFormattingService.Clear(script);

        const string expectedScript = "select * from Table;";

        updatedScript.Should().Be(expectedScript);
    }
    
    [Fact]
    public void Clear_MultipleLinesComment_TwoComment_InTheMiddleAndInTheBegin_Test()
    {
        const string script =
            """
            /* have some comments 
                that need to be removed*/
            select *
            /* have some comments 
                that need to be removed*/
            from Table;
            """;

        var updatedScript = scriptFormattingService.Clear(script);

        const string expectedScript = "select * from Table;";

        updatedScript.Should().Be(expectedScript);
    }
    
    [Fact]
    public void Clear_MultipleLinesComment_TwoComment_InTheMiddleAndInTheEnd_Test()
    {
        const string script =
            """
            select *
            /* have some comments 
                that need to be removed*/
            from Table;
            /* have some comments 
                that need to be removed*/
            """;

        var updatedScript = scriptFormattingService.Clear(script);

        const string expectedScript = "select * from Table;";

        updatedScript.Should().Be(expectedScript);
    }
    
    [Fact]
    public void Clear_MultipleLinesComment_TwoComment_InTheBeginAndInTheEnd_Test()
    {
        const string script =
            """
            /* have some comments 
                that need to be removed*/
            select *
            from Table;
            /* have some comments 
                that need to be removed*/
            """;

        var updatedScript = scriptFormattingService.Clear(script);

        const string expectedScript = "select * from Table;";

        updatedScript.Should().Be(expectedScript);
    }

    [Fact]
    public void Clear_MultipleLinesComment_TwoComment_OneInvalid_Test()
    {
        const string script =
            """
            /* have some comments 
                that need to be removed*/
            select *
            from Table;
            /* have some comments 
                that need to be removed
            """;

        var updatedScript = scriptFormattingService.Clear(script);
        
        updatedScript.Should().BeNull();
    }
    
    [Fact]
    public void Clear_SingleLineComment_TwoComment_Test()
    {
        const string script =
            """
            select * -- comment here
            from Table; -- comment here
            """;

        var updatedScript = scriptFormattingService.Clear(script);
        
        const string expectedScript = "select * from Table;";

        updatedScript.Should().Be(expectedScript);    
    }
    
    [Fact]
    public void Clear_SingleLineComment_Test()
    {
        const string script =
            """
            select * -- comment here
            from Table;
            """;

        var updatedScript = scriptFormattingService.Clear(script);
        
        const string expectedScript = "select * from Table;";

        updatedScript.Should().Be(expectedScript);    
    }

    [Fact]
    public void Clear_Tabs_Test()
    {
        const string script =
            """
            select * -- comment here
                from Table;
            """;

        var updatedScript = scriptFormattingService.Clear(script);
        
        const string expectedScript = "select * from Table;";

        updatedScript.Should().Be(expectedScript);
    }

    [Fact]
    public void Clear_WhiteSpaces_Test()
    {
        const string script =
            """
            select        * -- comment here
            from Table;
            """;

        var updatedScript = scriptFormattingService.Clear(script);
        
        const string expectedScript = "select * from Table;";

        updatedScript.Should().Be(expectedScript);
    }
}