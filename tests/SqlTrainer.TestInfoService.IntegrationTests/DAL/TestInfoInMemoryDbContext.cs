﻿using System.Collections.Concurrent;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;
using SqlTrainer.TestInfoService.Persistence;

namespace SqlTrainer.TestInfoService.IntegrationTests;

[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class TestInfoInMemoryDbContext(DbContextOptions<TestInfoDbContext> options) 
    : TestInfoDbContext(options)
{
    private static readonly ConcurrentDictionary<string, DbContextOptions<TestInfoDbContext>> Options = [];
    
    public static TestInfoDbContext Create(string key, Action<TestInfoDbContext>? seed = null)
    {
        if (Options.TryGetValue(key, out var options))
            return new TestInfoDbContext(options);
        
        options = new DbContextOptionsBuilder<TestInfoDbContext>()
            .UseInMemoryDatabase(key)
            .Options;
        
        var dbContext = new TestInfoDbContext(options);

        seed?.Invoke(dbContext);

        dbContext.SaveChanges();

        Options.TryAdd(key, options);
        
        return dbContext;
    }
}
