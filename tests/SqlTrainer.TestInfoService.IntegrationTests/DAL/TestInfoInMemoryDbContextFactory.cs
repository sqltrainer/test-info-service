﻿using Microsoft.EntityFrameworkCore;
using SqlTrainer.TestInfoService.Application.DAL;
using SqlTrainer.TestInfoService.Persistence;

namespace SqlTrainer.TestInfoService.IntegrationTests;

public sealed class TestInfoInMemoryDbContextFactory(string key)
{
    private readonly DbContextOptions<TestInfoDbContext> options = new DbContextOptionsBuilder<TestInfoDbContext>()
        .UseInMemoryDatabase(key)
        .Options;

    public ITestInfoDbContext Create() => new TestInfoDbContext(options);
}