﻿using Moq;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using SqlTrainer.TestInfoService.Domain.Models;
using SqlTrainer.TestInfoService.Application.Questions;

namespace SqlTrainer.TestInfoService.IntegrationTests;

public partial class QuestionTests
{
    public class AddTests : QuestionTests
    {
        private const string QuestionBody = "Add Question Test Question Body";
        private const string CorrectAnswerBody = "Add Question Test Correct Answer Body";
        private const int Complexity = 3;
        private static readonly Guid DatabaseId = Guid.NewGuid();
        
        private readonly AddQuestionRequestHandler sut;
        private readonly Topic? topic;

        public AddTests()
        {
            var loggerMock = new Mock<ILogger<AddQuestionRequestHandler>>();
            var dbContextFactory = new TestInfoInMemoryDbContextFactory("AddQuestion");
            
            using var dbContext = TestInfoInMemoryDbContext.Create("AddQuestion");
            topic = new Topic { Name = "Add Question Test Topic" };
            dbContext.Topics.Add(topic);
            dbContext.SaveChanges();
            
            sut = new AddQuestionRequestHandler(dbContextFactory.Create(), loggerMock.Object);
        }
        
        [Fact]
        public async Task Should_AddQuestion_Successfully()
        {
            // Arrange
            var addQuestionRequest = new AddQuestionRequest(
                QuestionBody, 
                Complexity,
                topic!.Id, 
                DatabaseId,
                CorrectAnswerBody);
            
            // Act
            var result = await sut.HandleAsync(addQuestionRequest);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            await using var context = TestInfoInMemoryDbContext.Create("AddQuestion");
            
            var question = await context.Questions
                .Include(q => q.CorrectAnswer)
                .FirstOrDefaultAsync(q => q.Id == result.Value);
            
            question.Should().NotBeNull();
            question!.Body.Should().Be(QuestionBody);
            question.Complexity.Should().Be(Complexity);
            question.TopicId.Should().Be(topic.Id);
            question.DatabaseId.Should().Be(DatabaseId);
            question.CorrectAnswer.Should().NotBeNull();
            question.CorrectAnswer!.Body.Should().Be(CorrectAnswerBody);
        }
    }
}