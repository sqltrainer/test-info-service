﻿using Moq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Questions;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests;

public partial class QuestionTests
{
    public class DeleteTests : QuestionTests
    {
        private readonly TestInfoInMemoryDbContextFactory dbContextFactory;
        
        private readonly DeleteQuestionRequestHandler sut;
        
        private readonly Question question;

        public DeleteTests()
        {
            var loggerMock = new Mock<ILogger<DeleteQuestionRequestHandler>>();
            dbContextFactory = new TestInfoInMemoryDbContextFactory("DeleteQuestion");
            
            using var dbContext = dbContextFactory.Create();
            
            var topic = new Topic { Name = "Delete Question Test Topic" };
            
            question = new Question
            {
                Body = "Delete Question Test Question",
                Complexity = 5,
                TopicId = topic.Id,
                DatabaseId = Guid.NewGuid()
            }.WithTopic(topic);
            dbContext.Questions.Add(question);

            dbContext.SaveChanges();
            
            sut = new DeleteQuestionRequestHandler(dbContextFactory.Create(), loggerMock.Object);
        }
        
        [Fact]
        public async Task Should_DeleteQuestion_Successfully()
        {
            // Arrange
            var deleteQuestionRequest = new DeleteQuestionRequest(question.Id);
            
            // Act
            var result = await sut.HandleAsync(deleteQuestionRequest);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            await using var context = dbContextFactory.Create();
            
            var deletedQuestion = await context.Questions
                .FirstOrDefaultAsync(q => q.Id == question.Id);
            deletedQuestion.Should().BeNull();
        }
    }
}