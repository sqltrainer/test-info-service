﻿using Moq;
using Application;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Questions;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests;

public partial class QuestionTests
{
    public class GetAllTests : QuestionTests
    {
        private static readonly Guid DatabaseId = Guid.NewGuid();
        
        private static readonly string[] QuestionBodies = Enumerable.Range(1, 500)
            .Select(i => $"Question _question_ Get All Test {i.ToString().PadLeft(3, '0')} Body - {Guid.NewGuid()}")
            .ToArray();
        
        private static readonly string[] CorrectAnswerBodies = Enumerable.Range(1, 500)
            .Select(i => $"Question _question_ Get All Test {i.ToString().PadLeft(3, '0')} Correct Answer Body - {Guid.NewGuid()}")
            .ToArray();

        private readonly GetAllQuestionsRequestHandler sut;

        public GetAllTests()
        {
            var loggerMock = new Mock<ILogger<GetAllQuestionsRequestHandler>>();
            var dbContextFactory = new TestInfoInMemoryDbContextFactory("GetAllQuestions");
            
            sut = new GetAllQuestionsRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            var topic = new Topic { Name = "Get All Test Topic" };

            var questions = Enumerable.Range(0, 500)
                .Select(i =>
                {
                    var random = new Random();
                            
                    var question = new Question
                    {
                        Body = QuestionBodies[i],
                        TopicId = topic.Id,
                        Complexity = random.Next(1, 5),
                        DatabaseId = DatabaseId
                    };
                    
                    question.WithCorrectAnswer(new CorrectAnswer
                    {
                        Body = CorrectAnswerBodies[i],
                        QuestionId = question.Id
                    });

                    question.WithTopic(topic);

                    return question;
                });
                    
            dbContext.Questions.AddRange(questions);
            dbContext.SaveChanges();
        }
        
        [Fact]
        public async Task Should_GetQuestions_By_question_SearchTermSortAscByBodyAndRetrieve4PageWith100Elements_Successfully()
        {
            // Arrange
            var request = new GetAllQuestionsRequest("_question_", "body", SortDirection.Ascending, 4, 100);
            
            // Act
            var result = await sut.HandleAsync(request);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Value.Should().NotBeEmpty();
            result.Value.Should().HaveCount(100);
            result.Value.First().CorrectAnswer.Should().NotBeNull();
            var questionNames = result.Value.Select(q => q.Body).ToArray();
            var expectedQuestionNames = QuestionBodies.Order().Skip(300).Take(100);
            questionNames.Should().BeEquivalentTo(expectedQuestionNames);
        }
    }
}