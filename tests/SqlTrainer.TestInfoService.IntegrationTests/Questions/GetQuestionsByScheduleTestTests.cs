﻿using Moq;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Questions;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests;

public partial class QuestionTests
{
    public class GetByScheduleTestTests : QuestionTests
    {
        private static readonly Guid DatabaseId = Guid.NewGuid();
        
        private static readonly string[] QuestionBodies = Enumerable.Range(1, 500)
            .Select(i => $"Question _question_ Get All Test {i.ToString().PadLeft(3, '0')} Body - {Guid.NewGuid()}")
            .ToArray();
        
        private static readonly string[] CorrectAnswerBodies = Enumerable.Range(1, 500)
            .Select(i => $"Question _question_ Get All Test {i.ToString().PadLeft(3, '0')} Correct Answer Body - {Guid.NewGuid()}")
            .ToArray();

        private readonly GetQuestionsByScheduleTestRequestHandler sut;

        private readonly Guid scheduleTestId;

        public GetByScheduleTestTests()
        {
            var loggerMock = new Mock<ILogger<GetQuestionsByScheduleTestRequestHandler>>();
            var dbContextFactory = new TestInfoInMemoryDbContextFactory("GetQuestionsByScheduleTest");
            
            sut = new GetQuestionsByScheduleTestRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            var topic = new Topic { Name = "Get All Test Topic" };

            var questions = Enumerable.Range(0, 500)
                .Select(i =>
                {
                    var random = new Random();
                    
                    var question = new Question
                    {
                        Body = QuestionBodies[i],
                        TopicId = topic.Id,
                        Complexity = random.Next(1, 5),
                        DatabaseId = DatabaseId
                    };
                    
                    question.WithCorrectAnswer(new CorrectAnswer
                    {
                        Body = CorrectAnswerBodies[i],
                        QuestionId = question.Id
                    });

                    question.WithTopic(topic);

                    return question;
                })
                .ToArray();
            
            dbContext.Questions.AddRange(questions);
            
            var test1 = new Test { Name = $"GetByScheduleTest Question Test Test - {Guid.NewGuid()}", CreatedAt = DateTime.Now };
            var test2 = new Test { Name = $"GetByScheduleTest Question Test Test - {Guid.NewGuid()}", CreatedAt = DateTime.Now };

            var startAt = DateTime.Now.AddDays(1);
            var finishAt = DateTime.Now.AddDays(1).AddHours(2);
            var scheduleTest1 = new ScheduleTest { TestId = test1.Id, StartAt = startAt, FinishAt = finishAt }.WithTest(test1);
            var scheduleTest2 = new ScheduleTest { TestId = test2.Id, StartAt = startAt, FinishAt = finishAt }.WithTest(test2);
            
            dbContext.ScheduleTests.AddRange(scheduleTest1, scheduleTest2);
            
            scheduleTestId = scheduleTest1.Id;
            
            var count = 0;
            var random = new Random();
            foreach (var question in questions)
            {
                var countMod = count % 3;
                count++;
                if (countMod == 0)
                    continue;
    
                var test = countMod == 1 ? test1 : test2;
                var testQuestion = new TestQuestion(test.Id, question.Id) { MaxMark = random.Next(1, 5) };
                
                if (test == test1)
                    test1.AddTestQuestion(testQuestion);
                else
                    test2.AddTestQuestion(testQuestion);
            }
            
            dbContext.SaveChanges();
        }
        
        [Fact]
        public async Task Should_GetQuestions_ByScheduleTestId_Successfully()
        {
            // Arrange
            var request = new GetQuestionsByScheduleTestRequest(scheduleTestId);
            
            // Act
            var result = await sut.HandleAsync(request);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Value.Should().NotBeEmpty();
            result.Value.Should().HaveCount(167);
            result.Value.First().CorrectAnswer.Should().NotBeNull();
            var questionNames = result.Value.Select(q => q.Body).Order().ToArray();
            var expectedQuestionNames = QuestionBodies.Where((_, i) => i % 3 == 1).Order();
            questionNames.Should().BeEquivalentTo(expectedQuestionNames);
        }
    }
}