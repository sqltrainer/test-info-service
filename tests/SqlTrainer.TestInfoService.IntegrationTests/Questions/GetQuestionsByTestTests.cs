﻿using Moq;
using Application;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Questions;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests;

public partial class QuestionTests
{
    public class GetByTestTests : QuestionTests
    {
        private static readonly Guid DatabaseId = Guid.NewGuid();
        
        private static readonly string[] QuestionBodies = Enumerable.Range(1, 500)
            .Select(i => $"Question _question_ Get All Test {i.ToString().PadLeft(3, '0')} Body - {Guid.NewGuid()}")
            .ToArray();
        
        private static readonly string[] CorrectAnswerBodies = Enumerable.Range(1, 500)
            .Select(i => $"Question _question_ Get All Test {i.ToString().PadLeft(3, '0')} Correct Answer Body - {Guid.NewGuid()}")
            .ToArray();

        private readonly GetQuestionsByTestRequestHandler sut;

        private readonly Guid testId;

        public GetByTestTests()
        {
            var loggerMock = new Mock<ILogger<GetQuestionsByTestRequestHandler>>();
            var dbContextFactory = new TestInfoInMemoryDbContextFactory("GetQuestionsByTest");
            
            sut = new GetQuestionsByTestRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            var topic = new Topic { Name = "Get All Test Topic" };

            var questions = Enumerable.Range(0, 500)
                .Select(i =>
                {
                    var random = new Random();
                    
                    var question = new Question
                    {
                        Body = QuestionBodies[i],
                        TopicId = topic.Id,
                        Complexity = random.Next(1, 5),
                        DatabaseId = DatabaseId
                    };
                    
                    question.WithCorrectAnswer(new CorrectAnswer
                    {
                        Body = CorrectAnswerBodies[i],
                        QuestionId = question.Id
                    });

                    question.WithTopic(topic);
                    
                    return question;
                })
                .ToArray();
            
            dbContext.Questions.AddRange(questions);
            
            var test1 = new Test { Name = $"GetByScheduleTest Question Test Test - {Guid.NewGuid()}", CreatedAt = DateTime.Now };
            var test2 = new Test { Name = $"GetByScheduleTest Question Test Test - {Guid.NewGuid()}", CreatedAt = DateTime.Now };

            dbContext.Tests.AddRange(test1, test2);

            testId = test1.Id;

            var count = 0;
            var random = new Random();
            foreach (var question in questions)
            {
                var countMod = count % 3;
                count++;
                if (countMod == 0)
                    continue;
    
                var test = countMod == 1 ? test1 : test2;
                var testQuestion = new TestQuestion(test.Id, question.Id) { MaxMark = random.Next(1, 5) };
                
                if (countMod == 1)
                    test1.AddTestQuestion(testQuestion);
                else
                    test2.AddTestQuestion(testQuestion);
            }

            dbContext.SaveChanges();
        }
        
        [Fact]
        public async Task Should_GetQuestions_ByTestIdSearchTerm_question_SortedByAsc3PageWith50Elements_Successfully()
        {
            // Arrange
            var getByTestQuestionsRequest = new GetQuestionsByTestRequest(testId, "_question_", "body", SortDirection.Ascending, 3, 50);
            
            // Act
            var result = await sut.HandleAsync(getByTestQuestionsRequest);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Value.Should().NotBeEmpty();
            result.Value.Should().HaveCount(50);
            result.Value.First().CorrectAnswer.Should().NotBeNull();
            var questionNames = result.Value.Select(q => q.Body).ToArray();
            var expectedQuestionNames = QuestionBodies.Where((_, i) => i % 3 == 1).Order().Skip(100).Take(50);
            questionNames.Should().BeEquivalentTo(expectedQuestionNames);
        }
    }
}