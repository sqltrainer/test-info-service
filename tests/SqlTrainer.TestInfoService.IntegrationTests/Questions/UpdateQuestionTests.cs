﻿using Moq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Questions;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests;

public partial class QuestionTests
{
    public class UpdateTests : QuestionTests
    {
        private const int Complexity = 5;
        private const string QuestionBody = "Update Question Test Question";
        private const string CorrectAnswerBody = "Update Question Test Correct Answer Body";
        private const string UpdatedQuestionBody = "Update Question Test Question Updated";
        private const string UpdatedCorrectAnswerBody = "Correct Answer Updated Test Body";
        
        private readonly Guid databaseId = Guid.NewGuid();

        private readonly TestInfoInMemoryDbContextFactory dbContextFactory;
        
        private readonly UpdateQuestionRequestHandler sut;
        
        private readonly Topic topic;
        private readonly Question question;

        public UpdateTests()
        {
            var loggerMock = new Mock<ILogger<UpdateQuestionRequestHandler>>();
            dbContextFactory = new TestInfoInMemoryDbContextFactory("UpdateQuestion");
            
            sut = new UpdateQuestionRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var context = dbContextFactory.Create();
            
            topic = new Topic { Name = "Update Question Test Topic" };
                    
            question = new Question
            {
                Body = QuestionBody,
                Complexity = Complexity,
                TopicId = topic.Id,
                DatabaseId = databaseId
            };
            
            question.WithTopic(topic);
            
            var correctAnswer = new CorrectAnswer
            {
                Body = CorrectAnswerBody,
                QuestionId = question.Id
            };
            question.WithCorrectAnswer(correctAnswer);
            
            context.Questions.Add(question);
            context.SaveChanges();
        }
        
        [Fact]
        public async Task Should_UpdateQuestion_Successfully()
        {
            // Arrange
            var updateQuestionRequest = new UpdateQuestionRequest(question.Id, UpdatedQuestionBody, Complexity, topic.Id, databaseId, UpdatedCorrectAnswerBody);
            
            // Act
            var result = await sut.HandleAsync(updateQuestionRequest);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            await using var context = dbContextFactory.Create();
            
            var updatedQuestion = await context.Questions
                .Include(q => q.CorrectAnswer)
                .FirstOrDefaultAsync(q => q.Id == question.Id);
            updatedQuestion.Should().NotBeNull();
            updatedQuestion!.Body.Should().Be(UpdatedQuestionBody);
            updatedQuestion.Complexity.Should().Be(Complexity);
            updatedQuestion.TopicId.Should().Be(topic.Id);
            updatedQuestion.DatabaseId.Should().Be(databaseId);
            updatedQuestion.CorrectAnswer.Should().NotBeNull();
            updatedQuestion.CorrectAnswer!.Body.Should().Be(UpdatedCorrectAnswerBody);
        }
    }
}