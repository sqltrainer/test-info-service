﻿using Moq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.ScheduleTests;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.ScheduleTests;

public partial class ScheduleTestServiceTests
{
    public class AddTests : ScheduleTestServiceTests
    {
        private readonly TestInfoInMemoryDbContextFactory dbContextFactory;
        private readonly AddScheduleTestRequestHandler sut;
        
        private readonly Test test;
        
        private readonly Guid user1Id = Guid.NewGuid();
        private readonly Guid user2Id = Guid.NewGuid();
        
        public AddTests()
        {
            var loggerMock = new Mock<ILogger<AddScheduleTestRequestHandler>>();
            dbContextFactory = new TestInfoInMemoryDbContextFactory("AddScheduleTestTest");
            
            sut = new AddScheduleTestRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            test = new Test { Name = "Add schedule test" };
            dbContext.Tests.Add(test);
            dbContext.SaveChanges();
        }
        
        [Fact]
        public async Task Should_AddScheduleTest_Successfully()
        {
            // Arrange
            var start = DateTimeOffset.UtcNow.AddDays(1);
            var end = start.AddHours(2);
            var request = new AddScheduleTestRequest(test.Id, start, end, [user1Id, user2Id]);
            
            // Act
            var result = await sut.HandleAsync(request);
            
            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Value.Should().NotBeEmpty();
            
            await using var dbContext = dbContextFactory.Create();
            
            var scheduleTest = await dbContext.ScheduleTests.Include(st => st.UserScheduleTests).FirstOrDefaultAsync(st => st.Id == result.Value);
            scheduleTest.Should().NotBeNull();
            scheduleTest!.TestId.Should().Be(test.Id);
            scheduleTest.StartAt.Should().BeCloseTo(start, TimeSpan.Zero);
            scheduleTest.FinishAt.Should().BeCloseTo(end, TimeSpan.Zero);
            scheduleTest.UserScheduleTests.Should().HaveCount(2);
            scheduleTest.UserScheduleTests.Should().Contain(ust => ust.UserId == user1Id);
            scheduleTest.UserScheduleTests.Should().Contain(ust => ust.UserId == user2Id);
        }
    }
}