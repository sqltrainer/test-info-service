﻿using Moq;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.ScheduleTests;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.ScheduleTests;

public partial class ScheduleTestServiceTests
{
    public class DeleteTests : ScheduleTestServiceTests
    {
        private readonly TestInfoInMemoryDbContextFactory dbContextFactory;
        private readonly DeleteScheduleTestRequestHandler sut;
        
        private readonly ScheduleTest scheduleTest;
        
        public DeleteTests()
        {
            var loggerMock = new Mock<ILogger<DeleteScheduleTestRequestHandler>>();
            dbContextFactory = new TestInfoInMemoryDbContextFactory("DeleteScheduleTestTest");

            sut = new DeleteScheduleTestRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            var test = new Test { Name = "Delete Schedule Test Test" };
                    
            scheduleTest = new ScheduleTest
            {
                TestId = test.Id, 
                StartAt = DateTimeOffset.UtcNow.AddDays(1),
                FinishAt = DateTimeOffset.UtcNow.AddDays(1).AddHours(2)
            }.WithTest(test);
            dbContext.ScheduleTests.Add(scheduleTest);
            dbContext.SaveChanges();
        }

        [Fact]
        public async Task Should_DeleteScheduleTest_Successfully()
        {
            // Arrange
            var request = new DeleteScheduleTestRequest(scheduleTest.Id);
            
            // Act
            var result = await sut.HandleAsync(request);
            
            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            await using var context = dbContextFactory.Create();
            
            var foundScheduleTest = await context.ScheduleTests.FindAsync(scheduleTest.Id);
            foundScheduleTest.Should().BeNull();
        }
    }
}