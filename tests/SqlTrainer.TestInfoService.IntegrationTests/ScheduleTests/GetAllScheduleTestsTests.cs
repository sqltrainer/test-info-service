﻿using Moq;
using Application;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.ScheduleTests;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.ScheduleTests;

public partial class ScheduleTestServiceTests
{
    public class GetAllTests : ScheduleTestServiceTests
    {
        private readonly GetAllScheduleTestsRequestHandler sut;
        
        private readonly IReadOnlyCollection<ScheduleTest> scheduleTests;
        
        public GetAllTests()
        {
            var loggerMock = new Mock<ILogger<GetAllScheduleTestsRequestHandler>>();
            var dbContextFactory = new TestInfoInMemoryDbContextFactory("GetAllScheduleTestsTests");

            sut = new GetAllScheduleTestsRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            var test = new Test { Name = "Get Schedule Tests" };
                    
            Random random = new();

            scheduleTests = Enumerable.Range(1, 500)
                .Select(_ =>
                {
                    var day = random.Next(1, 30);
                    var hour = random.Next(0, 23);
                    var minute = random.Next(0, 59);
                    return new ScheduleTest
                    {
                        TestId = test.Id,
                        StartAt = DateTimeOffset.UtcNow.AddDays(day).AddHours(hour).AddMinutes(minute),
                        FinishAt = DateTimeOffset.UtcNow.AddDays(day).AddHours(hour + 1).AddMinutes(minute + 30)
                    }.WithTest(test);
                })
                .ToArray();
                    
            dbContext.ScheduleTests.AddRange(scheduleTests);
            dbContext.SaveChanges();
        }

        [Fact]
        public async Task Should_RetrieveScheduleTests_OrderedByStartAtDescendingOnly5PageWith25Items_Successfully()
        {
            // Arrange
            var request = new GetAllScheduleTestsRequest(string.Empty, "startAt", SortDirection.Descending, 5, 25);

            // Act
            var result = await sut.HandleAsync(request);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Value.Should().HaveCount(25);
            result.Value.Select(st => st.StartAt).Should().BeInDescendingOrder();
            var expectedScheduleTests = scheduleTests.OrderByDescending(st => st.StartAt).Skip(100).Take(25).ToArray();
            result.Value.Should().BeEquivalentTo(expectedScheduleTests);
        }
    }
}