﻿using Moq;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.ScheduleTests;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.ScheduleTests;

public partial class ScheduleTestServiceTests
{
    public class GetByIdTests : ScheduleTestServiceTests
    {
        private readonly TestInfoInMemoryDbContextFactory dbContextFactory;
        private readonly GetByIdScheduleTestRequestHandler sut;
        
        private readonly ScheduleTest scheduleTest;
        
        public GetByIdTests()
        {
            var loggerMock = new Mock<ILogger<GetByIdScheduleTestRequestHandler>>();
            dbContextFactory = new TestInfoInMemoryDbContextFactory("GetScheduleTestById");

            sut = new GetByIdScheduleTestRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            var test = new Test { Name = "Get Schedule Test By Id Test" };
                    
            scheduleTest = new ScheduleTest
            {
                TestId = test.Id, 
                StartAt = DateTimeOffset.UtcNow.AddDays(1),
                FinishAt = DateTimeOffset.UtcNow.AddDays(1).AddHours(2)
            }.WithTest(test);
            dbContext.ScheduleTests.Add(scheduleTest);
            dbContext.SaveChanges();
        }

        [Fact]
        public async Task Should_RetrieveScheduleTestById_Successfully()
        {
            // Arrange
            var request = new GetByIdScheduleTestRequest(scheduleTest.Id);
            
            // Act
            var result = await sut.HandleAsync(request);
            
            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            await using var context = dbContextFactory.Create();
            
            var foundScheduleTest = await context.ScheduleTests.FindAsync(scheduleTest.Id);
            foundScheduleTest.Should().NotBeNull();
            foundScheduleTest!.TestId.Should().Be(scheduleTest.TestId);
            foundScheduleTest.StartAt.Should().BeCloseTo(scheduleTest.StartAt, TimeSpan.Zero);
            foundScheduleTest.FinishAt.Should().BeCloseTo(scheduleTest.FinishAt, TimeSpan.Zero);
        }
    }
}