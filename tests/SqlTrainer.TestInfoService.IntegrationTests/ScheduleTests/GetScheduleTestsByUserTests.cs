﻿using Moq;
using Application;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.ScheduleTests;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.ScheduleTests;

public partial class ScheduleTestServiceTests
{
    public class GetTestsByUser : ScheduleTestServiceTests
    {
        private readonly GetByUserScheduleTestsRequestHandler sut;
        
        private readonly Guid[] userIds = Enumerable.Range(1, 10).Select(_ => Guid.NewGuid()).ToArray();
        private readonly IReadOnlyCollection<ScheduleTest> scheduleTests;
        
        public GetTestsByUser()
        {
            var loggerMock = new Mock<ILogger<GetByUserScheduleTestsRequestHandler>>();
            var dbContextFactory = new TestInfoInMemoryDbContextFactory("GetScheduleTestsByUser");
            
            sut = new GetByUserScheduleTestsRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            var test = new Test { Name = "Get Schedule Tests By User" };
                    
            Random random = new();
                    
            scheduleTests = Enumerable.Range(1, 500)
                .Select(_ =>
                {
                    var day = random.Next(1, 30);
                    var hour = random.Next(0, 23);
                    var minute = random.Next(0, 59);
                            
                    return new ScheduleTest
                    {
                        TestId = test.Id,
                        StartAt = DateTimeOffset.UtcNow.AddDays(day).AddHours(hour).AddMinutes(minute),
                        FinishAt = DateTimeOffset.UtcNow.AddDays(day).AddHours(hour + 1).AddMinutes(minute + 30)
                    }.WithTest(test);
                })
                .ToArray();
            
            foreach (var scheduleTest in scheduleTests)
            {
                var numberOfUsers = random.Next(1, 5);
                var scheduleTestUserIds = Enumerable.Range(0, numberOfUsers)
                    .Select(_ => random.Next(0, userIds.Length))
                    .Distinct()
                    .Select(i => userIds[i]);
                
                dbContext.UserScheduleTests.AddRange(scheduleTestUserIds.Select(userId => new UserScheduleTest(scheduleTest.Id, userId).WithScheduleTest(scheduleTest)));
            }
                    
            dbContext.SaveChanges();
        }

        [Fact]
        public async Task Should_RetrieveScheduleTests_ByUserIdOrderedByStartAtDescendingOnly3PageWith20Items_Successfully()
        {
            // Arrange
            var userId = userIds[0];
            var request = new GetByUserScheduleTestsRequest(userId, string.Empty, "startAt", SortDirection.Descending, 3, 20);

            // Act
            var result = await sut.HandleAsync(request);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Value.Should().HaveCount(20);
            result.Value.Select(st => st.StartAt).Should().BeInDescendingOrder();
            var expectedScheduleTests = scheduleTests.Where(st => st.UserScheduleTests.Any(ust => ust.UserId == userId)).OrderByDescending(st => st.StartAt).Skip(40).Take(20).ToArray();
            result.Value.Should().BeEquivalentTo(expectedScheduleTests);
        }
    }
}