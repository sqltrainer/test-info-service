﻿using Moq;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.ScheduleTests;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.ScheduleTests;

public partial class ScheduleTestServiceTests
{
    public class UpdateTests : ScheduleTestServiceTests
    {
        private readonly TestInfoInMemoryDbContextFactory dbContextFactory;
        private readonly UpdateScheduleTestRequestHandler sut;

        private readonly Test test;
        private readonly ScheduleTest scheduleTest;
        
        public UpdateTests()
        {
            var loggerMock = new Mock<ILogger<UpdateScheduleTestRequestHandler>>();
            dbContextFactory = new TestInfoInMemoryDbContextFactory("UpdateScheduleTestTest");
            
            sut = new UpdateScheduleTestRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            test = new Test { Name = "Update Schedule Test Test" };
                    
            scheduleTest = new ScheduleTest
            {
                TestId = test.Id, 
                StartAt = DateTimeOffset.UtcNow.AddDays(1),
                FinishAt = DateTimeOffset.UtcNow.AddDays(1).AddHours(2)
            }.WithTest(test);
            dbContext.ScheduleTests.Add(scheduleTest);
            dbContext.SaveChanges();
        }

        [Fact]
        public async Task Should_MoveScheduleTestToTomorrow_Successfully()
        {
            // Arrange
            var request = new UpdateScheduleTestRequest(scheduleTest.Id, test.Id, DateTimeOffset.UtcNow.AddDays(2), DateTimeOffset.UtcNow.AddDays(2).AddHours(2), [Guid.NewGuid()]);
            
            // Act
            var result = await sut.HandleAsync(request);
            
            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            await using var context = dbContextFactory.Create();
            
            var foundScheduleTest = await context.ScheduleTests.FindAsync(scheduleTest.Id);
            foundScheduleTest.Should().NotBeNull();
            foundScheduleTest!.TestId.Should().Be(test.Id);
            foundScheduleTest.StartAt.Should().BeCloseTo(request.StartAt, TimeSpan.Zero);
            foundScheduleTest.FinishAt.Should().BeCloseTo(request.FinishAt, TimeSpan.Zero);
        }
    }
}