﻿using System.Collections;

namespace SqlTrainer.TestInfoService.IntegrationTests.Structures;

public sealed class IndexedQueue<T>(IEnumerable<T>? collection = null) : IEnumerable<T>
{
    private readonly List<T> items = collection?.ToList() ?? [];
    private int currentIndex;
    
    public int Count => items.Count;
    
    public T this[int index] => items[index];
    
    public void Add(T item) => items.Add(item);

    public T Next()
    {
        var item = items[currentIndex];
        currentIndex = (currentIndex + 1) % items.Count;
        return item;
    }
    
    public IEnumerator<T> GetEnumerator() => items.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}