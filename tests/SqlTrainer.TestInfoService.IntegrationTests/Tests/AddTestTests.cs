﻿using Moq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Tests;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.Tests;

public partial class TestServiceTests
{
    public class AddTests : TestServiceTests
    {
        private readonly TestInfoInMemoryDbContextFactory dbContextFactory;
        private readonly AddTestRequestHandler sut;

        private readonly Question question;
        
        public AddTests()
        {
            var loggerMock = new Mock<ILogger<AddTestRequestHandler>>();
            dbContextFactory = new TestInfoInMemoryDbContextFactory("AddTestTest");
            
            sut = new AddTestRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            var topic = new Topic { Name = "Add test topic" };
                    
            question = new Question
            {
                Body = "Add test question",
                TopicId = topic.Id,
                Complexity = 1,
                DatabaseId = Guid.NewGuid()
            }.WithTopic(topic);
            dbContext.Questions.Add(question);
            dbContext.SaveChanges();
        }
        
        [Fact]
        public async Task Add_Success()
        {
            // Arrange
            var addTestRequest = new AddTestRequest("Some test name", DateTimeOffset.UtcNow, [(question.Id, 3.0)]);
            
            // Act
            var result = await sut.HandleAsync(addTestRequest);
            
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            await using var dbContext = dbContextFactory.Create();
            
            var test = await dbContext.Tests.FirstOrDefaultAsync(t => t.Id == result.Value);
            test.Should().NotBeNull();
            test!.Name.Should().Be(addTestRequest.Name);
        }
    }
}