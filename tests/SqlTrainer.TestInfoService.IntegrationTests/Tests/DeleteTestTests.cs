﻿using Moq;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Tests;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.Tests;

public partial class TestServiceTests
{
    public class DeleteTests : TestServiceTests
    {
        private readonly TestInfoInMemoryDbContextFactory dbContextFactory;
        private readonly DeleteTestRequestHandler sut;
        
        private readonly Test test;
        
        public DeleteTests()
        {
            var loggerMock = new Mock<ILogger<DeleteTestRequestHandler>>();
            dbContextFactory = new TestInfoInMemoryDbContextFactory("DeleteTest");
            
            sut = new DeleteTestRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            test = new Test { Name = "Delete test" };
            dbContext.Tests.Add(test);
            dbContext.SaveChanges();
        }
        
        [Fact]
        public async Task Delete_Success()
        {
            // Arrange
            var deleteTestRequest = new DeleteTestRequest(test.Id);

            // Act
            var result = await sut.HandleAsync(deleteTestRequest);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            await using var dbContext = dbContextFactory.Create();
            
            var foundTest = await dbContext.Tests.FindAsync(test.Id);
            foundTest.Should().BeNull();
        }
    }
}