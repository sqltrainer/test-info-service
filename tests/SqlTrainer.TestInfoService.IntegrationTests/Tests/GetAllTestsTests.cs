﻿using Moq;
using Application;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Tests;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.Tests;

public partial class TestServiceTests
{
    public class GetAllTests : TestServiceTests
    {
        private readonly GetAllTestsRequestHandler sut;

        private readonly IReadOnlyCollection<Test> tests = Enumerable.Range(0, 500)
            .Select(i => new Test { Name = $"Test _test_ {i.ToString().PadLeft(3, '0')}" })
            .ToArray();
        
        public GetAllTests()
        {
            var loggerMock = new Mock<ILogger<GetAllTestsRequestHandler>>();
            var dbContextFactory = new TestInfoInMemoryDbContextFactory("GetAllTests");
            
            sut = new GetAllTestsRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            dbContext.Tests.AddRange(tests);
            dbContext.SaveChanges();
        }

        [Fact]
        public async Task GetAll_Success_ReturnTopicsResponse()
        {
            // Arrange
            var getAllTestsRequest = new GetAllTestsRequest("_test_", "name", SortDirection.Descending, 2, 50);

            // Act
            var result = await sut.HandleAsync(getAllTestsRequest);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Value.Should().HaveCount(50);
            
            var expectedNames = tests.OrderByDescending(t => t.Name).Skip(50).Take(50).Select(t => t.Name).ToArray();
            var actualNames = result.Value.Select(t => t.Name).ToArray();
            
            actualNames.Should().BeInDescendingOrder();
            actualNames.Should().BeEquivalentTo(expectedNames);
        }
    }
}