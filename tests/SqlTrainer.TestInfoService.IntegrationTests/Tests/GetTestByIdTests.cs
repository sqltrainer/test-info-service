﻿using Moq;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Tests;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.Tests;

public partial class TestServiceTests
{
    public class GetByIdTests : TestServiceTests
    {
        private readonly GetByIdTestRequestHandler sut;
        
        private readonly Test test;
        
        public GetByIdTests()
        {
            var loggerMock = new Mock<ILogger<GetByIdTestRequestHandler>>();
            var dbContextFactory = new TestInfoInMemoryDbContextFactory("GetByIdTest");
            
            sut = new GetByIdTestRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            test = new Test { Name = "GetById test" };
            dbContext.Tests.Add(test);
            dbContext.SaveChanges();
        }
        
        [Fact]
        public async Task GetById_Success()
        {
            // Arrange
            var getByIdTopicRequest = new GetByIdTestRequest(test.Id);

            // Act
            var result = await sut.HandleAsync(getByIdTopicRequest);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Value.Should().NotBeNull();
            result.Value.Name.Should().Be(test.Name);
        }
    }
}