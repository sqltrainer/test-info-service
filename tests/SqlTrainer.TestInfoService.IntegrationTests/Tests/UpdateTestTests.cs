﻿using Microsoft.EntityFrameworkCore;
using Moq;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Tests;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.Tests;

public partial class TestServiceTests
{
    public class UpdateTests : TestServiceTests
    {
        private readonly TestInfoInMemoryDbContextFactory dbContextFactory;
        private readonly UpdateTestRequestHandler sut;

        private readonly Question question;
        private readonly Test test;
        
        public UpdateTests()
        {
            var loggerMock = new Mock<ILogger<UpdateTestRequestHandler>>();
            dbContextFactory = new TestInfoInMemoryDbContextFactory("UpdateTest");
            
            sut = new UpdateTestRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            test = new Test { Name = "Update test" };
            var topic = new Topic { Name = "Update test topic" };
            question = new Question
            {
                Body = "Update test question",
                TopicId = topic.Id,
                Complexity = 1,
                DatabaseId = Guid.NewGuid()
            }.WithTopic(topic);
            
            dbContext.TestQuestions.Add(new TestQuestion(test.Id, question.Id) { MaxMark = 3.0 }.WithTest(test).WithQuestion(question));
            dbContext.SaveChanges();
        }
        
        [Fact]
        public async Task Update_Success()
        {
            // Arrange
            var updateTestRequest = new UpdateTestRequest(test.Id, "Update test name", [(question.Id, 5.0)]);

            // Act
            var result = await sut.HandleAsync(updateTestRequest);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            await using var dbContext = dbContextFactory.Create();
            
            var updatedTest = await dbContext.Tests.Include(t => t.TestQuestions).FirstOrDefaultAsync(t => t.Id == test.Id);
            updatedTest.Should().NotBeNull();
            updatedTest!.Name.Should().Be(updateTestRequest.Name);
            updatedTest.TestQuestions.Should().NotBeEmpty();
            updatedTest.TestQuestions.Count.Should().Be(1);
            updatedTest.TestQuestions.First().MaxMark.Should().Be(5.0);
        }
    }
}