﻿using Moq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Topics;

namespace SqlTrainer.TestInfoService.IntegrationTests.Topics;

public partial class TopicTests
{
    public class AddTests : TopicTests
    {
        private readonly TestInfoInMemoryDbContextFactory dbContextFactory;
        
        private readonly AddTopicRequestHandler sut;

        public AddTests()
        {
            var loggerMock = new Mock<ILogger<AddTopicRequestHandler>>();
            dbContextFactory = new TestInfoInMemoryDbContextFactory("AddTopic");
            
            sut = new AddTopicRequestHandler(dbContextFactory.Create(), loggerMock.Object);
        }
        
        [Fact]
        public async Task Should_AddTopic_Successfully()
        {
            // Arrange
            var addTopicRequest = new AddTopicRequest("Some test topic name");
            
            // Act
            var result = await sut.HandleAsync(addTopicRequest);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            await using var dbContext = dbContextFactory.Create();
            
            var topic = await dbContext.Topics.FirstOrDefaultAsync(t => t.Id == result.Value);
            topic.Should().NotBeNull();
            topic!.Name.Should().Be(addTopicRequest.Name);
        }
    }
}