﻿using Moq;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Topics;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.Topics;

public partial class TopicTests
{
    public class DeleteTests : TopicTests
    {
        private readonly TestInfoInMemoryDbContextFactory dbContextFactory;
        
        private readonly DeleteTopicRequestHandler sut;
        
        private readonly Topic topic;
        
        public DeleteTests()
        {
            var loggerMock = new Mock<ILogger<DeleteTopicRequestHandler>>();
            dbContextFactory = new TestInfoInMemoryDbContextFactory("DeleteTopic");
            
            sut = new DeleteTopicRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            topic = new Topic { Name = "Delete topic 1" };
            dbContext.Topics.Add(topic);
            dbContext.SaveChanges();
        }
        
        [Fact]
        public async Task Should_DeleteTopic_Successfully()
        {
            // Arrange
            var deleteTopicRequest = new DeleteTopicRequest(topic.Id);

            // Act
            var result = await sut.HandleAsync(deleteTopicRequest);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            await using var dbContext = dbContextFactory.Create();
            
            var foundTopic = await dbContext.Topics.FindAsync(topic.Id);
            foundTopic.Should().BeNull();
        }
    }
}