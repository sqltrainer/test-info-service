﻿using Moq;
using Application;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Topics;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.Topics;

public partial class TopicTests
{
    public class GetAllTests : TopicTests
    {
        private readonly GetAllTopicsRequestHandler sut;
        
        private static readonly Topic[] Topics = Enumerable.Range(1, 500).Select(i => new Topic
        {
            Name = $"Topic _topic_ Get All {i.ToString().PadLeft(3, '0')}"
        }).ToArray();
        
        public GetAllTests()
        {
            var loggerMock = new Mock<ILogger<GetAllTopicsRequestHandler>>();
            var dbContextFactory = new TestInfoInMemoryDbContextFactory("GetAllTopics");
            
            sut = new GetAllTopicsRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            dbContext.Topics.AddRange(Topics);
            dbContext.SaveChanges();
        }
        
        [Fact]
        public async Task Should_GetTopicsBySearchTermOrderedByNameDescendingOnly2PageWith50Items_Successfully()
        {
            // Arrange
            var getAllTopicsRequest = new GetAllTopicsRequest("Topic _topic_ Get All", "name", SortDirection.Descending, 2, 50);

            // Act
            var response = await sut.HandleAsync(getAllTopicsRequest);

            // Assert
            response.IsSucceeded.Should().BeTrue(string.Join(", ", response.Problems));
            response.Value.Should().NotBeNull();
            
            var actualNames = response.Value.Select(t => t.Name).ToArray();
            actualNames.Should().BeInDescendingOrder();
            
            var expectedNames = Topics.Select(t => t.Name).OrderDescending().Skip(50).Take(50).ToArray();
            actualNames.Should().BeEquivalentTo(expectedNames);
        }
    }
}