﻿using Moq;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Topics;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.Topics;

public partial class TopicTests
{
    public class GetByIdTests : TopicTests
    {
        private readonly GetByIdTopicRequestHandler sut;
    
        private readonly Topic topic;
    
        public GetByIdTests()
        {
            var loggerMock = new Mock<ILogger<GetByIdTopicRequestHandler>>();
            var dbContextFactory = new TestInfoInMemoryDbContextFactory("GetByIdTopic");
            
            sut = new GetByIdTopicRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            topic = new Topic { Name = "Get by id topic 1" };
            dbContext.Topics.Add(topic);
            dbContext.SaveChanges();
        }
        
        [Fact]
        public async Task Should_GetTopicById_Successfully()
        {
            // Arrange
            var getByIdTopicRequest = new GetByIdTopicRequest(topic.Id);

            // Act
            var result = await sut.HandleAsync(getByIdTopicRequest);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Value.Should().NotBeNull();
        }
    }
}