﻿using Moq;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Topics;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.Topics;

public partial class TopicTests
{
    public class UpdateTests : TopicTests
    {
        private readonly TestInfoInMemoryDbContextFactory dbContextFactory;
    
        private readonly UpdateTopicRequestHandler sut;
    
        private readonly Topic topic;
    
        public UpdateTests()
        {
            var loggerMock = new Mock<ILogger<UpdateTopicRequestHandler>>();
            dbContextFactory = new TestInfoInMemoryDbContextFactory("UpdateTopic");
            
            sut = new UpdateTopicRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            topic = new Topic { Name = "Update topic" };
            dbContext.Topics.Add(topic);
            dbContext.SaveChanges();
        }
        
        [Fact]
        public async Task Should_UpdateTopic_Successfully()
        {
            // Arrange
            var updateTopicRequest = new UpdateTopicRequest(topic.Id, $"Updated topic {topic.Id}");

            // Act
            var result = await sut.HandleAsync(updateTopicRequest);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            await using var dbContext = dbContextFactory.Create();
            
            var foundTopic = await dbContext.Topics.FindAsync(topic.Id);
            foundTopic.Should().NotBeNull();
            foundTopic!.Name.Should().Be(updateTopicRequest.Name);
        }
    }
}