﻿using Moq;
using Results;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DatabaseHelper.ScriptResults;
using SqlTrainer.TestInfoService.Application.Services;
using SqlTrainer.TestInfoService.Application.UserAnswers;
using SqlTrainer.TestInfoService.Domain.Models;
using SqlTrainer.TestInfoService.IntegrationTests.Structures;

namespace SqlTrainer.TestInfoService.IntegrationTests.UserAnswers;

public partial class UserAnswerServiceTests
{
    public class AddRangeTests : UserAnswerServiceTests
    {
        private static readonly Guid[] DatabaseIds = Enumerable.Range(0, 5).Select(_ => Guid.NewGuid()).ToArray();
        private static readonly Guid UserId = Guid.NewGuid();
        
        private static readonly string[] CorrectAnswerBodies = Enumerable.Range(0, 5)
            .Select(i => $"Add Answers Test Correct Answer {i} - {DatabaseIds[i]}")
            .ToArray();
        
        private static readonly string[] UserAnswerBodies = Enumerable.Range(0, 5)
            .Select(i => $"Add Answers Test User Answer {i} - {DatabaseIds[i]}")
            .ToArray();
        
        private static readonly IndexedQueue<double> Marks = new(Enumerable.Range(0, 5).Select(_ => new Random().NextDouble() * 3));
        
        private static readonly Mock<IScriptService> ScriptServiceMock = new();
        private static readonly Mock<IScriptFormattingService> ScriptFormattingServiceMock = new();
        private static readonly Mock<IScriptResultsCompareService> ScriptResultsCompareServiceMock = new();
        
        private readonly TestInfoInMemoryDbContextFactory dbContextFactory;
        private readonly AddUserAnswersRequestHandler sut;

        private readonly IReadOnlyCollection<Question> questions;
        private readonly ScheduleTest scheduleTest;
        
        public AddRangeTests()
        {
            var loggerMock = new Mock<ILogger<AddUserAnswersRequestHandler>>();
            dbContextFactory = new TestInfoInMemoryDbContextFactory("AddUserAnswers");
            
            sut = new AddUserAnswersRequestHandler(
                dbContextFactory.Create(), 
                ScriptServiceMock.Object, 
                ScriptFormattingServiceMock.Object, 
                ScriptResultsCompareServiceMock.Object, 
                loggerMock.Object);
            
            ScriptFormattingServiceMock
                .Setup(s => s.Clear(It.IsAny<string>()))
                .Returns<string>(s => s);

            var setSequenceMock = ScriptResultsCompareServiceMock
                .SetupSequence(s => s.Compare(It.IsAny<ScriptResult>(), It.IsAny<ScriptResult>(), It.IsAny<double>()));
            
            for (var i = 0; i < Marks.Count; i++)
            {
                setSequenceMock.Returns(Marks.Next());
                
                var expectedScript = new ScriptResult { Script = CorrectAnswerBodies[i] };
                var userScript = new ScriptResult { Script = UserAnswerBodies[i] };

                var result = Result.Create<IReadOnlyCollection<ScriptResult>>();
                result.Complete([userScript, expectedScript]);

                var index = i;

                ScriptServiceMock
                    .Setup(s => s.ExecuteScriptsAsync(new[] { UserAnswerBodies[index], CorrectAnswerBodies[index] }, DatabaseIds[index], It.IsAny<CancellationToken>()))
                    .ReturnsAsync(result);
            }
            
            using var dbContext = dbContextFactory.Create();
            
            var topic = new Topic { Name = "User Answer Test Topic" };
            var random = new Random();

            questions = Enumerable.Range(0, 5)
                .Select(i =>
                {
                    var question = new Question
                    {
                        Body = $"Add User Answer Test Question {i}",
                        TopicId = topic.Id,
                        Complexity = random.Next(1, 5),
                        DatabaseId = DatabaseIds[i]
                    };

                    question.WithCorrectAnswer(new CorrectAnswer
                    {
                        Body = CorrectAnswerBodies[i],
                        QuestionId = question.Id
                    });

                    question.WithTopic(topic);
                    
                    return question;
                })
                .ToArray();
            dbContext.Questions.AddRange(questions);
            
            var test = new Test
            {
                Name = "Add Answers Test", 
                CreatedAt = DateTimeOffset.UtcNow
            };

            test.AddTestQuestions(Enumerable.Range(0, 5)
                .Select(j =>
                {
                    var question = questions.ElementAt(j); 
                            
                    return new TestQuestion(test.Id, question.Id)
                    {
                        MaxMark = 3.0
                    }.WithQuestion(question);
                })
                .ToArray());
            
            dbContext.Tests.Add(test);
            
            scheduleTest = new ScheduleTest
            {
                TestId = test.Id,
                StartAt = DateTimeOffset.UtcNow.AddHours(-1),
                FinishAt = DateTimeOffset.UtcNow.AddHours(1)
            };

            scheduleTest.WithTest(test);
                            
            scheduleTest.WithUserScheduleTests([new UserScheduleTest(scheduleTest.Id, UserId)]);

            dbContext.ScheduleTests.Add(scheduleTest);
            
            dbContext.SaveChanges();
        }
        
        [Fact]
        public async Task Should_AddUserAnswers_Successfully()
        {
            // Arrange
            var request = new AddUserAnswersRequest(scheduleTest.Id, UserId, DateTimeOffset.UtcNow, questions
                .Select((q, i) => new QuestionBody(q.Id, UserAnswerBodies[i]))
                .ToArray());
            
            // Act
            var result = await sut.HandleAsync(request);
            
            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            await using var dbContext = dbContextFactory.Create();
            
            var userAnswers = await dbContext.UserAnswers
                .Where(ua => ua.ScheduleTestId == scheduleTest.Id && ua.UserId == UserId)
                .ToListAsync();
            
            userAnswers.Should().HaveCount(5);
            userAnswers.Should().OnlyContain(ua => UserAnswerBodies.Contains(ua.Body));
            for (var i = 0; i < userAnswers.Count; i++)
            {
                var userAnswer = userAnswers.ElementAt(i);
                userAnswer.ProgramMark.Should().Be(Marks[i]);
                userAnswer.TeacherMark.Should().BeNull();
            }
        }
    }
}