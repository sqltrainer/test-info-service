﻿using Moq;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Domain.Models;
using SqlTrainer.TestInfoService.Application.UserAnswers;

namespace SqlTrainer.TestInfoService.IntegrationTests.UserAnswers;

public partial class UserAnswerServiceTests
{
    public class GetUserAnswersByScheduleTestAndUserTests : UserAnswerServiceTests
    {
        private static readonly Guid[] DatabaseId = Enumerable.Range(0, 5).Select(_ => Guid.NewGuid()).ToArray();
        private static readonly Guid[] UserId = Enumerable.Range(0, 5).Select(_ => Guid.NewGuid()).ToArray();
        
        private readonly GetByScheduleTestAndUserAnswersRequestHandler sut;

        private readonly List<UserAnswer> answers = [];
        
        public GetUserAnswersByScheduleTestAndUserTests()
        {
            var loggerMock = new Mock<ILogger<GetByScheduleTestAndUserAnswersRequestHandler>>();
            var dbContextFactory = new TestInfoInMemoryDbContextFactory("GetUserAnswersByScheduleTestAndUser");
            
            sut = new GetByScheduleTestAndUserAnswersRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            var topic = new Topic { Name = "User Answer Test Topic" };
            var random = new Random();

            var questions = Enumerable.Range(0, 25).Select(i =>
            {
                var question = new Question
                {
                    Body = $"Update User Answer Test Question {i}",
                    TopicId = topic.Id,
                    Complexity = random.Next(1, 5),
                    DatabaseId = DatabaseId[i % 5]
                };

                question.WithTopic(topic);
                question.WithCorrectAnswer(new CorrectAnswer
                {
                    Body = $"Update User Answer Test Correct Answer {i}",
                    QuestionId = question.Id
                });

                return question;
            }).ToArray();

            dbContext.Questions.AddRange(questions);
            
            var scheduleTests = Enumerable.Range(0, 5).Select(i =>
            {
                var test = new Test
                {
                    Name = $"Add Answers Test {i}", 
                    CreatedAt = DateTimeOffset.UtcNow
                };

                test.AddTestQuestions(Enumerable.Range(0, 5).Select(j =>
                {
                    var question = questions.ElementAt(i * 5 + j);

                    return new TestQuestion(test.Id, question.Id)
                    {
                        MaxMark = random.Next(1, 5)
                    }.WithQuestion(question);
                }).ToArray());

                var scheduleTest = new ScheduleTest
                {
                    TestId = test.Id,
                    StartAt = DateTimeOffset.UtcNow.AddHours(-1),
                    FinishAt = DateTimeOffset.UtcNow.AddHours(1)
                };
                
                scheduleTest.WithTest(test);
                scheduleTest.WithUserScheduleTests([new UserScheduleTest(scheduleTest.Id, UserId[i])]);

                return scheduleTest;
            });
            
            foreach (var scheduleTest in scheduleTests)
            {
                foreach (var userScheduleTest in scheduleTest.UserScheduleTests)
                {
                    foreach (var testQuestion in scheduleTest.Test!.TestQuestions)
                    {
                        var answer = new UserAnswer(scheduleTest.Id, userScheduleTest.UserId, testQuestion.QuestionId)
                        {
                            Body = $"Update User Answer Test Body {Guid.NewGuid()}",
                            AnsweredAt = DateTimeOffset.UtcNow,
                            ProgramMark = random.Next(1, 5)
                        };
                        
                        answer.WithQuestion(testQuestion.Question!);
                        answer.WithUserScheduleTest(userScheduleTest);
                        
                        answers.Add(answer);
                    }
                }
            }
            
            dbContext.UserAnswers.AddRange(answers);
            
            dbContext.SaveChanges();
        }
        
        [Fact]
        public async Task Should_GetAnswersByScheduleTestAndUser_Successfully()
        {
            // Arrange
            var testAnswer = answers.First();
            var request = new GetByScheduleTestAndUserAnswersRequest(testAnswer.ScheduleTestId, testAnswer.UserId);
            
            // Act
            var result = await sut.HandleAsync(request);
            
            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            var expectedAnswers = answers
                .Where(a => a.ScheduleTestId == testAnswer.ScheduleTestId && a.UserId == testAnswer.UserId)
                .ToArray();
            
            result.Value.Should().BeEquivalentTo(expectedAnswers);
        }
    }
}