﻿using Moq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Domain.Models;
using SqlTrainer.TestInfoService.Application.UserAnswers;

namespace SqlTrainer.TestInfoService.IntegrationTests.UserAnswers;

public partial class UserAnswerServiceTests
{
    public class UpdateAnswerTests : UserAnswerServiceTests
    {
        private static readonly Guid DatabaseId = Guid.NewGuid();
        private static readonly Guid UserId = Guid.NewGuid();
        
        private readonly TestInfoInMemoryDbContextFactory dbContextFactory;
        private readonly UpdateUserAnswerRequestHandler sut;

        private readonly UserAnswer answer;
        
        public UpdateAnswerTests()
        {
            var loggerMock = new Mock<ILogger<UpdateUserAnswerRequestHandler>>();
            dbContextFactory = new TestInfoInMemoryDbContextFactory("UpdateUserAnswer");
            
            sut = new UpdateUserAnswerRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            var random = new Random();
            
            var topic = new Topic { Name = "User Answer Test Topic" };

            var question = new Question
            {
                Body = "Update User Answer Test Question",
                TopicId = topic.Id,
                Complexity = random.Next(1, 5),
                DatabaseId = DatabaseId
            };

            question.WithCorrectAnswer(new CorrectAnswer
            {
                Body = "Update User Answer Test Correct Answer",
                QuestionId = question.Id
            }).WithTopic(topic);
            
            var test = new Test
            {
                Name = "Add Answers Test", 
                CreatedAt = DateTimeOffset.UtcNow
            };

            var testQuestion = new TestQuestion(test.Id, question.Id)
            {
                MaxMark = 3.0
            }.WithQuestion(question);
            test.AddTestQuestion(testQuestion);
            
            var scheduleTest = new ScheduleTest
            {
                TestId = test.Id,
                StartAt = DateTimeOffset.UtcNow.AddHours(-1),
                FinishAt = DateTimeOffset.UtcNow.AddHours(1)
            }.WithTest(test);

            var userScheduleTest = new UserScheduleTest(scheduleTest.Id, UserId);
            userScheduleTest.WithScheduleTest(scheduleTest);

            answer = new UserAnswer(userScheduleTest.ScheduleTest!.Id, question.Id, userScheduleTest.UserId)
            {
                Body = "Update User Answer Test Body",
                AnsweredAt = DateTimeOffset.UtcNow,
                ProgramMark = 3.0
            }.WithQuestion(question).WithUserScheduleTest(userScheduleTest);
            
            dbContext.UserAnswers.Add(answer);
            
            dbContext.SaveChanges();
        }
        
        [Fact]
        public async Task Should_UpdateUserAnswerTeacherMark_Successfully()
        {
            // Arrange
            var request = new UpdateUserAnswerRequest(answer.ScheduleTestId, answer.UserId, answer.QuestionId, 2.0);
            
            // Act
            var result = await sut.HandleAsync(request);
            
            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            await using var dbContext = dbContextFactory.Create();
            
            var userAnswer = await dbContext.UserAnswers
                .FirstOrDefaultAsync(a => a.ScheduleTestId == answer.ScheduleTestId && a.UserId == answer.UserId && a.QuestionId == answer.QuestionId);
            
            userAnswer.Should().NotBeNull();
            userAnswer!.ProgramMark.Should().Be(3.0);
            userAnswer.TeacherMark.Should().Be(2.0);
        }
    }
}