﻿using Moq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.UserScheduleTests;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.IntegrationTests.UserScheduleTests;

public class UserScheduleTestTests
{
    public class UpdateUserScheduleTestTests : UserScheduleTestTests
    {
        private readonly TestInfoInMemoryDbContextFactory dbContextFactory;
        private readonly UpdateUserScheduleTestRequestHandler sut;
        
        private readonly ScheduleTest scheduleTest;
        private readonly Guid userIdThatFinishedTest;
        private readonly Guid userIdThatCheckedByTeacher;
        
        public UpdateUserScheduleTestTests() 
        {
            var loggerMock = new Mock<ILogger<UpdateUserScheduleTestRequestHandler>>();
            dbContextFactory = new TestInfoInMemoryDbContextFactory("UpdateUserScheduleTest");
            
            sut = new UpdateUserScheduleTestRequestHandler(dbContextFactory.Create(), loggerMock.Object);
            
            using var dbContext = dbContextFactory.Create();
            
            var test = new Test
            {
                Name = "Update User Schedule Test",
                CreatedAt = DateTimeOffset.UtcNow
            };

            scheduleTest = new ScheduleTest
            {
                TestId = test.Id,
                StartAt = DateTimeOffset.UtcNow.AddHours(-1),
                FinishAt = DateTimeOffset.UtcNow.AddHours(1),
            }.WithTest(test);
            
            userIdThatFinishedTest = Guid.NewGuid();
            userIdThatCheckedByTeacher = Guid.NewGuid();

            var userScheduleTestJustFinished = new UserScheduleTest(scheduleTest.Id, userIdThatFinishedTest).WithScheduleTest(scheduleTest);
            var userScheduleTestNotCheckedByTeacherYet = new UserScheduleTest(scheduleTest.Id, userIdThatCheckedByTeacher).WithScheduleTest(scheduleTest);
            userScheduleTestNotCheckedByTeacherYet.UserFinishedTest(DateTimeOffset.UtcNow.AddSeconds(-30));
                    
            dbContext.UserScheduleTests.AddRange(userScheduleTestJustFinished, userScheduleTestNotCheckedByTeacherYet);
            dbContext.SaveChanges();
        }
        
        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public async Task Update_Success(bool checkedByTeacher)
        {
            // Arrange
            var finishedAt = DateTimeOffset.UtcNow;
            var userId = checkedByTeacher ? userIdThatCheckedByTeacher : userIdThatFinishedTest;
            var request = new UpdateUserScheduleTestRequest(scheduleTest.Id, userId, finishedAt, checkedByTeacher ? checkedByTeacher : null);
            
            // Act
            var result = await sut.HandleAsync(request);
            
            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            
            await using var context = dbContextFactory.Create();
            
            var userScheduleTest = await context.UserScheduleTests
                .FirstOrDefaultAsync(ust => ust.ScheduleTestId == scheduleTest.Id && ust.UserId == userId);
            
            userScheduleTest.Should().NotBeNull();
            
            if (checkedByTeacher)
                userScheduleTest!.FinishedAt!.Value.Should().NotBe(finishedAt);
            else
                userScheduleTest!.FinishedAt!.Value.Should().Be(finishedAt);
            
            if (checkedByTeacher)
                userScheduleTest.CheckedByTeacher.Should().Be(checkedByTeacher);
            else
                userScheduleTest.CheckedByTeacher.Should().BeNull();
        }
    }
}